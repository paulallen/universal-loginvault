﻿using LoginVault.Models;
using System.Threading.Tasks;
using Windows.Storage;

namespace LoginVault.Serialization
{
    public interface IModelLoader
    {
        Task<Vault> Load(IStorageFile file, string password);
        Task Save(Vault vault, IStorageFile file, string password);
    }
}
