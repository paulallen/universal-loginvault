﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;

namespace LoginVault.Serialization
{
    public class MyFileWriter
    {
#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped
        public async Task ToFile(String payload, IStorageFile file)
#pragma warning restore S4457 // Parameter validation in "async"/"await" methods should be wrapped
        {
            if (payload == null) throw new ArgumentNullException(nameof(payload));
            if (file == null) throw new ArgumentNullException(nameof(file));

            using (var stream = (await file.OpenAsync(FileAccessMode.ReadWrite)).AsStream())
            {
                stream.SetLength(0);

                var writer = new MyStreamWriter();

                await writer.WriteV2(stream, payload);
            }
        }

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped
        public async Task<String> FromFile(IStorageFile file)
#pragma warning restore S4457 // Parameter validation in "async"/"await" methods should be wrapped
        {
            if (file == null) throw new ArgumentNullException(nameof(file));

            using (var stream = (await file.OpenAsync(FileAccessMode.Read)).AsStream())
            {
                var writer = new MyStreamWriter();

                return await writer.Read(stream);
            }
        }
    }
}
