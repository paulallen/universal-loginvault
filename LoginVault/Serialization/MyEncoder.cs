﻿using LoginVault.Encryption;
using System;
using Windows.Security.Cryptography;

namespace LoginVault.Serialization
{
    public class MyEncoder
    {
        public String ToEncoded(String plaintext, String password)
        {
            if (plaintext == null) throw new ArgumentNullException(nameof(plaintext));
            if (password == null) throw new ArgumentNullException(nameof(password));

            var encryptor = new Encrypter();

            var ibCipher = encryptor.Encrypt(plaintext, password);
            string payload = CryptographicBuffer.EncodeToBase64String(ibCipher);
            return payload;
        }

        public String FromEncoded(String cipher, String password)
        {
            if (cipher == null) throw new ArgumentNullException(nameof(cipher));
            if (password == null) throw new ArgumentNullException(nameof(password));

            var encryptor = new Encrypter();

            var ibCipher = CryptographicBuffer.DecodeFromBase64String(cipher);
            var decoded = encryptor.Decrypt(ibCipher, password);
            return decoded;
        }
    }
}
