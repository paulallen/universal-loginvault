﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LoginVault.Serialization
{
    internal class MyStreamWriter
    {
        public async Task Write(Stream stream, string payload)
        {
            using StreamWriter sw = new StreamWriter(stream);
            await sw.WriteLineAsync(V1_HEADER);

            var lines = SplitIntoLines(payload);
            for (int i = 0; i < lines.Count; ++i)
            {
                await sw.WriteLineAsync(lines[i]);
            }

            await sw.WriteLineAsync(V1_FOOTER);

            sw.Flush();
        }

        public async Task WriteV2(Stream stream, string payload)
        {
            using StreamWriter sw = new StreamWriter(stream);
            await sw.WriteLineAsync(V2_HEADER);

            var lines = SplitIntoLines(payload);
            for (int i = 0; i < lines.Count; ++i)
            {
                await sw.WriteLineAsync(lines[i]);
            }

            await sw.WriteLineAsync(V2_FOOTER);

            sw.Flush();
        }

        private List<string> SplitIntoLines(string payload)
        {
            var lines = new List<string>();
            int nLength = payload.Length;
            for (int nIndex = 0; nIndex < nLength; nIndex += 64)
            {
                int nBlockSize = 64;
                if (nLength - nIndex < nBlockSize)
                {
                    nBlockSize = nLength - nIndex;
                }

                string strLine = payload.Substring(nIndex, nBlockSize);
                lines.Add(strLine);
            }
            return lines;
        }

        public async Task<string> Read(Stream stream)
        {
            using StreamReader sr = new StreamReader(stream);
            string strHeader = await sr.ReadLineAsync();

            if (strHeader == V1_HEADER || strHeader == V2_HEADER)
            {
                StringBuilder sbPayload = new StringBuilder();
                string strBlock;
                do
                {
                    strBlock = await sr.ReadLineAsync();
                    if (strBlock != V1_FOOTER && strBlock != V2_FOOTER)
                    {
                        if (strBlock.Length <= 64)
                        {
                            sbPayload.Append(strBlock);
                        }
                        else
                        {
                            throw new FormatException("Read() - Invalid file format");
                        }
                    }
                } while (strBlock != V1_FOOTER && strBlock != V2_FOOTER);

                return sbPayload.ToString();
            }
            else
            {
                throw new FormatException("Read() - Invalid file format");
            }
        }

        private const string V1_HEADER = "===== BEGIN LOGINVAULT V1 =====";
        private const string V1_FOOTER = "===== END LOGINVAULT V1 =====";

        private const string V2_HEADER = "===== BEGIN LOGINVAULT V2 =====";
        private const string V2_FOOTER = "===== END LOGINVAULT V2 =====";
    }
}
