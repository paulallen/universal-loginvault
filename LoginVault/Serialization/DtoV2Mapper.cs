﻿using LoginVault.DtosV2;
using LoginVault.Models;
using System;

namespace LoginVault.Serialization
{
    public class DtoV2Mapper
    {
        public DtoVault ToDto(Vault modelVault)
        {
            if (modelVault == null) throw new ArgumentNullException(nameof(modelVault));

            DtoVault dtoVault = new DtoVault();

            foreach (Folder modelFolder in modelVault.Folders)
            {
                DtoFolder dtoFolder = new DtoFolder
                {
                    Name = modelFolder.Name
                };

                foreach (Card modelCard in modelFolder.Cards)
                {
                    dtoFolder.Cards.Add(new DtoCard
                    {
                        Url = modelCard.Url,
                        Username = modelCard.Username,
                        Password = modelCard.Password,
                        Other = modelCard.Other
                    });
                }

                dtoVault.Folders.Add(dtoFolder);
            }

            return dtoVault;
        }

        public Vault FromDto(DtoVault dtoVault)
        {
            if (dtoVault == null) throw new ArgumentNullException(nameof(dtoVault));

            Vault vault = new Vault();

            foreach (DtoFolder dtoFolder in dtoVault.Folders)
            {
                Folder modelFolder = new Folder
                {
                    Name = dtoFolder.Name
                };

                foreach (DtoCard card in dtoFolder.Cards)
                {
                    modelFolder.Cards.Add(new Card
                    {
                        Url = card.Url,
                        Username = card.Username,
                        Password = card.Password,
                        Other = card.Other
                    });
                }

                vault.Folders.Add(modelFolder);
            }

            vault.NotChanged();

            return vault;
        }
    }
}
