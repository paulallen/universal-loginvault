﻿using LoginVault.Models;
using System;
using System.Threading.Tasks;
using Windows.Storage;

namespace LoginVault.Serialization
{
    public class ModelLoader : IModelLoader
    {
#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped
        public async Task Save(Vault vault, IStorageFile file, string password)
#pragma warning restore S4457 // Parameter validation in "async"/"await" methods should be wrapped
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));
            if (file == null) throw new ArgumentNullException(nameof(file));
            if (password == null) throw new ArgumentNullException(nameof(password));

            var fw = new MyFileWriter();
            var en = new MyEncoder();
            var serial = new MySerializer();

            var plaintext = serial.ToString(vault);
            var encoded = en.ToEncoded(plaintext, password);
            await fw.ToFile(encoded, file);
        }

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped
        public async Task<Vault> Load(IStorageFile file, string password)
#pragma warning restore S4457 // Parameter validation in "async"/"await" methods should be wrapped
        {
            if (file == null) throw new ArgumentNullException(nameof(file));
            if (password == null) throw new ArgumentNullException(nameof(password));

            var fw = new MyFileWriter();
            var en = new MyEncoder();
            var serial = new MySerializer();

            var cipher = await fw.FromFile(file);
            var decoded = en.FromEncoded(cipher, password);
            var vault = serial.FromString(decoded);
            return vault;
        }
    }
}
