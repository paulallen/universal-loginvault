﻿using LoginVault.Models;
using System;

namespace LoginVault.Serialization
{
    public class DtoMapper
    {
        public Dtos.Box ToDto(Vault vault)
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));

            Dtos.Box box = new Dtos.Box();

            foreach (Folder folder in vault.Folders)
            {
                Dtos.Folder dtoFolder = new Dtos.Folder
                {
                    Name = folder.Name
                };

                foreach (Card card in folder.Cards)
                {
                    dtoFolder.Cards.Add(new Dtos.Card
                    {
                        Url = card.Url,
                        Username = card.Username,
                        Password = card.Password,
                        Other = card.Other
                    });
                }

                box.Folders.Add(dtoFolder);
            }

            return box;
        }

        public Vault FromDto(Dtos.Box box)
        {
            if (box == null) throw new ArgumentNullException(nameof(box));

            Vault vault = new Vault();

            foreach (Dtos.Folder folder in box.Folders)
            {
                Folder modelFolder = new Folder
                {
                    Name = folder.Name
                };

                foreach (Dtos.Card card in folder.Cards)
                {
                    modelFolder.Cards.Add(new Card
                    {
                        Url = card.Url,
                        Username = card.Username,
                        Password = card.Password,
                        Other = card.Other
                    });
                }

                vault.Folders.Add(modelFolder);
            }

            vault.NotChanged();

            return vault;
        }
    }
}
