﻿using LoginVault.Dtos;
using LoginVault.DtosV2;
using LoginVault.Models;
using System;

namespace LoginVault.Serialization
{
    public class MySerializer
    {
        public String ToString(Vault vault)
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));

            var mapper = new DtoV2Mapper();
            var serializer = new DtoSerializer();

            var dtoVault = mapper.ToDto(vault);
            var raw = serializer.Serialize(dtoVault);
            return raw;
        }

        public Vault FromString(String decoded)
        {
            if (decoded == null) throw new ArgumentNullException(nameof(decoded));

            if (decoded.StartsWith("<", StringComparison.Ordinal))
            {
                var serializer = new BoxSerializer();
                var mapper = new DtoMapper();

                var box = serializer.Deserialize(decoded);
                var vault = mapper.FromDto(box);
                return vault;
            }
            else if (decoded.StartsWith("{", StringComparison.Ordinal))
            {
                var serialv2 = new DtoSerializer();
                var mapper = new DtoV2Mapper();

                var dtovault = serialv2.Deserialize(decoded);
                var vault = mapper.FromDto(dtovault);
                return vault;
            }
            else
            {
                throw new ArgumentException("Argh!");
            }
        }
    }
}
