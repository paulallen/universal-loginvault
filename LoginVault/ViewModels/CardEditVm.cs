﻿using LoginVault.Models;
using LoginVault.Mvvm;
using LoginVault.Services;

namespace LoginVault.ViewModels
{
    public class CardEditVm : Observable
    {
        public Card Card { get; private set; }

        public CardEditVm(Card card)
        {
            Card = card;

            _url = card?.Url ?? "";
            _username = card?.Username ?? "";
            _password = card?.Password ?? "";
            _repeatPassword = card?.Password ?? "";
            _other = card?.Other ?? "";

            _difficultyMessage = "";
            _errorMessage = "";
        }

        private string _url;
        public string Url
        {
            get { return _url; }
            set { SetField(ref _url, value); }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetField(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetField(ref _password, value); }
        }

        private string _repeatPassword;
        public string RepeatPassword
        {
            get { return _repeatPassword; }
            set { SetField(ref _repeatPassword, value); }
        }

        private string _other;
        public string Other
        {
            get { return _other; }
            set { SetField(ref _other, value); }
        }

        private string _difficultyMessage;
        public string PasswordDiffcultyMessage
        {
            get { return _difficultyMessage; }
            set { SetField(ref _difficultyMessage, value); }
        }

        private string _errorMessage;
        public string PasswordErrorMessage
        {
            get { return _errorMessage; }
            set { SetField(ref _errorMessage, value); }
        }

        private void CheckPasswords()
        {
            PasswordDiffcultyMessage = helper.Rate(Password);
            PasswordErrorMessage = helper.CheckPasswords(Password, RepeatPassword);
        }

        private readonly IPasswordHelper helper = new PasswordHelper();
    }
}
