﻿using LoginVault.Mvvm;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace LoginVault.ViewModels
{
#nullable enable

    public class LayoutVm : Observable
    {
        private static readonly object s_padlock = new object();
        private static LayoutVm? s_instance;
        public static LayoutVm Instance
        {
            get
            {
                lock (s_padlock)
                {
                    if (s_instance == null)
                    {
                        s_instance = new LayoutVm();
                    }
                }

                return s_instance;
            }
        }

        private bool _showPasswords;
        public bool ShowPasswords
        {
            get { return _showPasswords; }
            set { SetField(ref _showPasswords, value); }
        }

        private bool _isPhone;
        public bool IsPhone
        {
            get { return _isPhone; }
            set { SetField(ref _isPhone, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetField(ref _title, value); }
        }

        private readonly SolidColorBrush _themeBrush;
        public Color ThemeColour
        {
            get { return _themeBrush.Color; }
            set { _themeBrush.Color = value; }
        }

        public LayoutVm()
        {
            _showPasswords = false;
            _isPhone = false;
            _title = "";
            _themeBrush = (SolidColorBrush)Application.Current.Resources["ThemeBrush"];
        }
    }
}
