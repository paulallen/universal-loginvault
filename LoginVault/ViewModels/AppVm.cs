﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Mvvm;
using LoginVault.Services;
using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Utils;
using Views;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace LoginVault.ViewModels
{
#nullable enable

    public class AppVm : Observable
    {
        private readonly IDialogService _dialogs = DialogService.Instance;
        private readonly IStorageService _storageService;
        private readonly IRecoveryFileService _recoveryFileService;
        private readonly CommandService _commandService;
        private readonly DragAndDropService _dragAndDropService;
        private readonly ExampleVaultService _exampleVaultService;
        private readonly AppData _appData;

        // XAML Binds to
        private Vault? _currentVault;
        public Vault? CurrentVault
        {
            get { return _currentVault; }
            set { SetField(ref _currentVault, value); }
        }

        private Folder? _currentFolder;
        public Folder? CurrentFolder
        {
            get { return _currentFolder; }
            set { SetField(ref _currentFolder, value); }
        }

        private Card? _currentCard;
        public Card? CurrentCard
        {
            get { return _currentCard; }
            set { SetField(ref _currentCard, value); }
        }

        private CardEditVm _displayedCard;
        public CardEditVm DisplayedCard
        {
            get { return _displayedCard; }
            set { SetField(ref _displayedCard, value); }
        }

        private bool _isEditingCard;
        public bool IsEditingCard
        {
            get { return _isEditingCard; }
            set { SetField(ref _isEditingCard, value); }
        }

        private bool _isSettingsVisible;
        public bool IsSettingsVisible
        {
            get { return _isSettingsVisible; }
            set { SetField(ref _isSettingsVisible, value); }
        }

        private UserControl _mainControl;
        public UserControl MainControl
        {
            get { return _mainControl; }
            private set { SetField(ref _mainControl, value); }
        }

        public ICommand CmdNew => new RelayCommand(async () => await New());
        public ICommand CmdOpen => new RelayCommand(async () => await PickAndLoad());
        public ICommand CmdSave => new RelayCommand(async () => await Save());
        public ICommand CmdSaveAs => new RelayCommand(async () => await PickAndSave());
        public ICommand CmdClose => new RelayCommand(Close);

        public ICommand CmdAddFolder => new RelayCommand(async () => await AddFolder());
        public ICommand CmdEditFolder => new RelayCommand(async () => await EditSelectedFolderName());
        public ICommand CmdDeleteFolder => new RelayCommand(DeleteSelectedFolder);

        public ICommand CmdAddCard => new RelayCommand(AddCard);
        public ICommand CmdEditDisplayedCardStart => new RelayCommand(EditDisplayedCardStart);
        public ICommand CmdEditDisplayedCardOk => new RelayCommand(EditDisplayedCardOk);
        public ICommand CmdEditDisplayedCardCancel => new RelayCommand(EditDisplayedCardEnd);
        public ICommand CmdDeleteSelectedCard => new RelayCommand(DeleteSelectedCard);

        public ICommand CmdUndo => new RelayCommand(() => _commandService.Undo());
        public ICommand CmdRedo => new RelayCommand(() => _commandService.Redo());
        public ICommand CmdCopyDisplayedCardPassword => new RelayCommand(CopyToClipboardDisplayedCardPassword);

        public AppVm(AppData appData, CommandService commandService, IStorageService storageService, IRecoveryFileService recoveryFileService)
        {
            _appData = appData ?? throw new ArgumentNullException(nameof(appData));
            _commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));
            _storageService = storageService ?? throw new ArgumentNullException(nameof(storageService));
            _recoveryFileService = recoveryFileService ?? throw new ArgumentNullException(nameof(recoveryFileService));

            _appData.PropertyChanged += OnAppDataPropertyChanged;
            _storageService.PropertyChanged += OnPropertyChanged;

            _dragAndDropService = new DragAndDropService(_appData, _commandService);
            _exampleVaultService = new ExampleVaultService(_appData, _commandService);

            _isSettingsVisible = false;
            _displayedCard = new CardEditVm(new Card());
            _mainControl = new DesktopVaultControl();
        }

        private void OnAppDataPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(AppData.CurrentVault))
            {
                CurrentVault = _appData.CurrentVault ?? new Vault();

                if (CurrentVault != null)
                {
                    CurrentVault.PropertyChanged += OnPropertyChanged;
                }
            }
            else if (e.PropertyName == nameof(AppData.CurrentFolder))
            {
                CurrentFolder = _appData.CurrentFolder;
            }
            else if (e.PropertyName == nameof(AppData.CurrentCard))
            {
                CurrentCard = _appData.CurrentCard;
                DisplayedCard = new CardEditVm(_appData.CurrentCard ?? new Card());
            }
            else
            {
                Logger.D($"Unhandled PropertyName: {e.PropertyName}");
            }
        }

        public async Task OnTimer()
        {
            await _recoveryFileService.AutoSave();
            ApplicationView.GetForCurrentView().Title = MakeTitle();
        }

        public async Task Initialize()
        {
            UserInteractionMode mode = UIViewSettings.GetForCurrentView().UserInteractionMode;
            LayoutVm.Instance.IsPhone = mode != UserInteractionMode.Mouse;

            if (LayoutVm.Instance.IsPhone)
            {
                SystemNavigationManager.GetForCurrentView().BackRequested += PhoneOnlyOnBackRequested;
                MainControl = new PhoneVaultControl();
            }

            // OOBE will only run the first time
            await _exampleVaultService.OfferExampleVault();
        }

        //
        // ListView Click to navigate
        //
        public void FolderClick(Folder folder)
        {
            _commandService.Do(new SelectFolder(folder));
        }

        public void CardClick(Card card)
        {
            _commandService.Do(new SelectCard(card));

            IsEditingCard = false;
        }

        //
        // Drag and Drop
        //
        public void DragItemsStarting(DragItemsStartingEventArgs e)
        {
            _dragAndDropService.DragItemStarting(e);
        }

        public static void DragOver(DragEventArgs e)
        {
            DragAndDropService.DragOver(e);
        }

        public void Drop(object sender, DragEventArgs e)
        {
            _dragAndDropService.Drop(sender, e);
        }

        //
        // File ops
        //
        private async Task PickAndLoad()
        {
            await _storageService.LoadPickedFile();

            var location = new CardLocation(CardLocation.None);
            _commandService.Do(new SelectLocation(location));
        }

        private async Task Save()
        {
            await _storageService.SaveCurrentFile();
        }

        private async Task PickAndSave()
        {
            await _storageService.SavePickedFile();
        }

        //
        // Settings
        //
        public void SetThemeColour(Color colour)
        {
            LayoutVm.Instance.ThemeColour = colour;

            // Seems to need a kick
            MainControl = LayoutVm.Instance.IsPhone
                ? (UserControl)new PhoneVaultControl()
                : (UserControl)new DesktopVaultControl();
        }

        //
        // Ops
        //
        private async Task New()
        {
            await _storageService.SaveOrDiscardChanges();
            Close();
        }

        private void Close()
        {
            _storageService.Clear();
            _commandService.Clear();

            _appData.CurrentVault.Clear();
            _appData.SelectedLocation = new CardLocation(CardLocation.None);

            IsEditingCard = false;
        }

        private void DeleteSelectedFolder()
        {
            if (_appData.CurrentVault == null) return;
            if (_appData.CurrentFolder == null) return;

            var folder = _appData.CurrentFolder;

            _commandService.Do(new Sequence(
                new CreateDeletedItems(),
                new SelectLocation(new CardLocation(_appData.CurrentVault.Folders.IndexOf(folder) - 1)),
                new DeleteFolder(_appData.CurrentVault.Folders.IndexOf(folder))
                ));
        }

        private async Task AddFolder()
        {
            string name = await _dialogs.ShowEditNameDialog("");
            if (!string.IsNullOrEmpty(name))
            {
                Folder folder = new Folder { Name = name };

                _commandService.Do(new Sequence(
                    new AddFolder(folder),
                    new SortFolders(),
                    new SelectFolder(folder)
                    ));
            }
        }

        private async Task EditSelectedFolderName()
        {
            if (_appData.CurrentVault == null) return;
            if (_appData.CurrentFolder == null) return;

            string name = await _dialogs.ShowEditNameDialog(_appData.CurrentFolder.Name);
            if (!string.IsNullOrEmpty(name))
            {
                var folder = _appData.CurrentFolder;

                _commandService.Do(new Sequence(
                    new RenameFolder(_appData.CurrentVault.Folders.IndexOf(folder), name),
                    new SortFolders(),
                    new SelectFolder(folder)
                    ));
            }
        }

        private void DeleteSelectedCard()
        {
            if (_appData.CurrentCard == null) return;

            var location = _appData.CurrentVault.FindCardLocationOfCard(_appData.CurrentCard);

            _commandService.Do(new Sequence(
                new CreateDeletedItems(),
                new SelectLocation(new CardLocation(location.FolderIndex, location.CardIndex - 1)),
                new DeleteCard(location.FolderIndex, location.CardIndex)
                ));

            if (LayoutVm.Instance.IsPhone)
            {
                MainControl = new PhoneVaultControl();
            }
        }

        //
        // Clipboard
        //
        private void CopyToClipboardDisplayedCardPassword()
        {
            if (DisplayedCard == null) return;
            if (DisplayedCard.Card == null) return;

            DragAndDropService.CopyTextToClipboard(DisplayedCard.Card.Password);
        }

        //
        // Phone navigation
        //
        public void PhoneOnlyNavigateToCard()
        {
            Assert.IsTrue(LayoutVm.Instance.IsPhone, "Aargh!");

            MainControl = new PhoneCardControl();
        }

        private void PhoneOnlyOnBackRequested(object sender, BackRequestedEventArgs e)
        {
            Assert.IsTrue(LayoutVm.Instance.IsPhone, "Aargh!");

            if (MainControl is PhoneCardControl)
            {
                EditDisplayedCardEnd();
                e.Handled = true;
            }
        }

        //
        // Common add/edit
        //
        private void AddCard()
        {
            if (_appData.CurrentFolder == null) return;

            var card = new Card();

            var seq = new Sequence(
                new AddCard(_appData.CurrentVault.Folders.IndexOf(_appData.CurrentFolder), card),
                new SelectCard(card)
                );

            _commandService.Do(seq);

            EditDisplayedCardStart();
        }

        private void EditDisplayedCardStart()
        {
            if (LayoutVm.Instance.IsPhone)
            {
                MainControl = new PhoneCardControl();
            }

            IsEditingCard = true;
        }

        private void EditDisplayedCardOk()
        {
            if (_appData.SelectedLocation == null) return;
            if (_appData.CurrentVault == null) return;
            if (_appData.CurrentFolder == null) return;
            if (_appData.CurrentCard == null) return;

            var newValues = new Card();
            newValues.Url = DisplayedCard.Url;
            newValues.Username = DisplayedCard.Username;
            newValues.Password = DisplayedCard.Password;
            newValues.Other = DisplayedCard.Other;

            _commandService.Do(new Sequence(
                new UpdateCard(_appData.SelectedLocation.FolderIndex, _appData.SelectedLocation.CardIndex, newValues),
                new SortCards(_appData.CurrentVault.Folders.IndexOf(_appData.CurrentFolder)),
                new SelectCard(_appData.CurrentCard)
                ));

            EditDisplayedCardEnd();
        }

        private void EditDisplayedCardEnd()
        {
            IsEditingCard = false;

            if (LayoutVm.Instance.IsPhone)
            {
                MainControl = new PhoneVaultControl();
            }
        }

        //
        // Title
        //
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CurrentVault.IsChanged) || e.PropertyName == nameof(NewStorageInfo.Path))
            {
                string title = MakeTitle();

                // Phone UI binds to this property
                LayoutVm.Instance.Title = title;

                // Application sets it
                ApplicationView.GetForCurrentView().Title = title;
            }
        }

        private string MakeTitle()
        {
            string title = "[no file]";

            if (_storageService.NewStorageInfo != null)
            {
                Assert.IsTrue(_storageService.NewStorageInfo.Path != null, "No Path");

                title = Path.GetFileName(_storageService.NewStorageInfo.Path);

                if (_storageService.NewStorageInfo.IsReadOnly)
                {
                    title += " [read only]";
                }
            }

            if (CurrentVault == null)
            {
                title += " Current Vault:  (null)";
            }
            else if (CurrentVault.IsChanged)
            {
                title += " [Is Changed]";
            }

            if (_storageService.NewStorageInfo == null)
            {
                title += " [No File]";
            }
            else if (!_storageService.IsSameAsLast)
            {
                title += " [Different File]";
            }

            if (_recoveryFileService.NewRecoveryInfo == null)
            {
                title += " [No Recovery File]";
            }
            else if (!_recoveryFileService.IsSameAsLast)
            {
                title += " [Different Recovery File]";
            }
            else
            {
                title += "[Active Recovery File]";
            }

            return title;
        }
    }
}
