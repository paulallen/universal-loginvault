﻿using LoginVault.Models;
using LoginVault.Serialization;
using LoginVault.Services;
using LoginVault.ViewModels;
using System;
using System.Threading;
using System.Threading.Tasks;
using Utils;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.UI.Xaml;

namespace LoginVault
{
    public sealed partial class App : Application, IDisposable
    {
        private readonly StoreFileLogger _loggerImplementation = new StoreFileLogger("LoginVault");
        private readonly AppData _appData;
        private readonly CommandService _commandService;
        private readonly StorageService _storageService;
        private readonly RecoveryFileService _recoveryFileService;

#nullable enable
        private AppVm? _viewModel;
        private MainPage? _mainPage;
        private Timer? _timer;
#nullable restore

        public App()
        {
            InitializeComponent();

            Logger.SetLogger(_loggerImplementation);

            var local = new LocalFolderService();
            var dialogs = DialogService.Instance;
            var popups = new PopupService();
            var pickers = new FilePicker();
            var loader = new ModelLoader();

            _appData = new AppData();
            _commandService = new CommandService(_appData);
            _storageService = new StorageService(_appData, _commandService, dialogs, popups, pickers, loader);
            _recoveryFileService = new RecoveryFileService(_appData, _commandService, local, _storageService, dialogs, loader);
            _storageService.RecoveryFileService = _recoveryFileService;

            Suspending += OnSuspending;
            Resuming += OnResuming;
            UnhandledException += OnUnhandledException;
        }

        private async void OnUnhandledException(object sender, Windows.UI.Xaml.UnhandledExceptionEventArgs e)
        {
            if (e == null) return;
            if (e.Exception == null) return;

            Logger.E(e.Exception.ToString());

            e.Handled = true;
            var popups = new PopupService();
            await popups.ShowUnhandledException(e.Exception);
        }

        protected async override void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (args == null) return;

            Logger.D($"OnLaunched() - Kind: '{args.Kind}', PreviousExecutionState: '{args.PreviousExecutionState}'");

            CreateState();

            // Don't dupe the OnResuming password dialog.
            if (!_isInOnResuming)
            {
                await InitializeVault();
            }
        }

        protected async override void OnFileActivated(FileActivatedEventArgs args)
        {
            base.OnFileActivated(args);

            if (args == null) return;
            if (args.Files == null) return;
            if (args.Files.Count == 0) return;

            Logger.D($"OnFileActivated() - Path: '{args.Files[0].Path}'");

            CreateState();

            StorageFile file = await StorageFile.GetFileFromPathAsync(args.Files[0].Path);
            await InitializeWithFile(file);
        }

        private void CreateState()
        {
            Logger.D("CreateState()");

            _viewModel = new AppVm(_appData, _commandService, _storageService, _recoveryFileService);
            _mainPage = new MainPage { DataContext = _viewModel };
            _timer = new Timer(Callback, _mainPage, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5));

            Window.Current.Content = _mainPage;
            Window.Current.Activate();
        }

        private static void Callback(object state)
        {
            MainPage page = (MainPage)state;
            _ = page.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => page.OnTimer());
        }

        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            Logger.D("OnSuspending()");
            if (_viewModel == null) throw new AssertionException("No ViewModel");

            var deferral = e.SuspendingOperation.GetDeferral();

            await _recoveryFileService.AutoSave();

            // Make sure no old data is visible if we re-launch
            _viewModel.CmdClose.Execute(0);

            Logger.Suspend();
            deferral.Complete();
        }

        private bool _isInOnResuming;

        private async void OnResuming(object sender, object e)
        {
            Logger.Resume();

            _isInOnResuming = true;
            Logger.D("OnResuming()");

            await InitializeVault();

            _isInOnResuming = false;
        }

        private async Task InitializeVault()
        {
            if (_viewModel == null) throw new AssertionException("No ViewModel");

            InitializeLayout();

            // Either load the recovery file,
            bool loaded = await _recoveryFileService.LoadRecoveryFile();
            if (!loaded)
            {
                // Or maybe the last file
                await _storageService.LoadLastFile();
            }

            await _viewModel.Initialize();
        }

        private async Task InitializeWithFile(IStorageFile file)
        {
            if (_viewModel == null) throw new AssertionException("No ViewModel");

            InitializeLayout();

            await _storageService.LoadFile(file);

            await _viewModel.Initialize();
        }

        static private void InitializeLayout()
        {
            LayoutVm.Instance.ShowPasswords = false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool isDisposed;
        void Dispose(bool disposing)
        {
            if (isDisposed) return;

            if (disposing)
            {
                _timer?.Dispose();
                _loggerImplementation?.Dispose();
            }

            isDisposed = true;
        }
    }
}
