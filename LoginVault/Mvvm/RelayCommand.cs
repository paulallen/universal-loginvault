﻿using System;
using System.Windows.Input;

namespace LoginVault.Mvvm
{
    /// <summary>
    /// void CmdXxxx() 
    /// </summary>
    public sealed class RelayCommand : ICommand
    {
#nullable enable
        public event EventHandler? CanExecuteChanged;
#nullable restore

        public RelayCommand(Action execute)
            : this(execute, () => true)
        {
            if (execute == null) { throw new ArgumentNullException(nameof(execute)); }
        }

        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            if (execute == null) { throw new ArgumentNullException(nameof(execute)); }

            _execute = execute;
            _canExecute = canExecute;
        }

        bool ICommand.CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute();
        }

        void ICommand.Execute(object parameter)
        {
            _execute();
        }

        public void InvokeCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        private readonly Action _execute;
        private readonly Func<bool> _canExecute;
    }

    /// <summary>
    /// void CmdXxxx(T t)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class RelayCommand<T> : ICommand
    {
#nullable enable
        public event EventHandler? CanExecuteChanged;
#nullable restore

        public RelayCommand(Action<T> execute)
            : this(execute, () => true)
        {
            if (execute == null) { throw new ArgumentNullException(nameof(execute)); }
        }

        public RelayCommand(Action<T> execute, Func<bool> canExecute)
        {
            if (execute == null) { throw new ArgumentNullException(nameof(execute)); }

            _execute = execute;
            _canExecute = canExecute;
        }

        bool ICommand.CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute();
        }

        void ICommand.Execute(object parameter)
        {
            _execute((T)parameter);
        }

        public void InvokeCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        private readonly Action<T> _execute;
        private readonly Func<bool> _canExecute;
    }
}
