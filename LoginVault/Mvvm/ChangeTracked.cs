﻿using System.Runtime.CompilerServices;

namespace LoginVault.Mvvm
{
    public abstract class ChangeTracked : Observable
    {
        public bool IsChanged
        {
            get { return _isChanged; }
            set { SetField(ref _isChanged, value); }
        }

        protected ChangeTracked()
            : base()
        {
            IsChanged = false;
        }

        protected void SetFieldTracked<T>(ref T field, T value, [CallerMemberName] string propertyName = "")
        {
            bool changed = field == null || field.Equals(value);
            SetField(ref field, value, propertyName);
            if (changed)
            {
                IsChanged = true;
            }
        }

        private bool _isChanged;
    }
}
