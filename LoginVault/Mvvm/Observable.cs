﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace LoginVault.Mvvm
{
    public abstract class Observable : INotifyPropertyChanged
    {
#nullable enable
        public event PropertyChangedEventHandler? PropertyChanged;
#nullable restore

        protected void InvokePropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (propertyName == null) { throw new ArgumentNullException(nameof(propertyName)); }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = "")
        {
            if (propertyName == null) { throw new ArgumentNullException(nameof(propertyName)); }

            field = value;
            InvokePropertyChanged(propertyName);
        }

        protected void SetProperty<T>(object target, T value, [CallerMemberName] string propertyName = "")
        {
            if (target == null) { throw new ArgumentNullException(nameof(target)); }
            if (propertyName == null) { throw new ArgumentNullException(nameof(propertyName)); }

            PropertyInfo propertyInfo = target.GetType().GetProperty(propertyName);
            if (propertyInfo == null) { throw new ArgumentException($"{nameof(propertyName)} is not a property"); }

            propertyInfo.SetValue(target, value);
            InvokePropertyChanged(propertyName);
        }
    }
}
