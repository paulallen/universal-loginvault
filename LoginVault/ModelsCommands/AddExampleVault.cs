﻿using LoginVault.Models;
using Utils;

namespace LoginVault.Commands
{
    public class AddExampleVault : BaseCommand
    {
        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - create example vault");

            _oldVault = new Vault(appData.CurrentVault);

            appData.CurrentVault.Clear();

            Folder internet = new Folder() { Name = "Internet" };
            internet.Cards.Add(new Card() { Url = "news.myfavourite.com", Username = "bert", Password = "iPod Catastrophe", Other = "" });
            internet.Cards.Add(new Card() { Url = "film.reviews.com", Username = "bert", Password = "Yellow B33tle", Other = "" });
            internet.Cards.Add(new Card() { Url = "social.gossip.com", Username = "bert", Password = "RampantSnail!", Other = "" });

            Folder utilities = new Folder() { Name = "Utilities" };
            utilities.Cards.Add(new Card() { Url = "email.com", Username = "bert", Password = "Carp3t Ph0ne1", Other = "Last reading: 123456" });
            utilities.Cards.Add(new Card() { Url = "my.electricity.com", Username = "bert", Password = "WendyHouse4", Other = "" });
            utilities.Cards.Add(new Card() { Url = "my.gas.co.uk", Username = "ethel", Password = "Proper Cabbage #", Other = "Phone password: gghq1i7" });
            utilities.Cards.Add(new Card() { Url = "my.phonecompany.com", Username = "bertandethel", Password = "waste tractor 5", Other = "" });
            utilities.Cards.Add(new Card() { Url = "my.water.co.uk", Username = "bert", Password = "Tower Blue", Other = "" });

            Folder banking = new Folder() { Name = "Banking" };
            banking.Cards.Add(new Card() { Url = "account.bank.com", Username = "bert", Password = "Pudding Fault27", Other = "Frist pet: Cat" });
            banking.Cards.Add(new Card() { Url = "buildings.society.co.uk", Username = "ethel", Password = "Fungus shirt 8", Other = "Date: 22/3/1970" });
            banking.Cards.Add(new Card() { Url = "bonds.gov.uk", Username = "bert", Password = "Giant Toothbrush", Other = "" });

            appData.CurrentVault.Folders.Add(internet);
            appData.CurrentVault.Folders.Add(utilities);
            appData.CurrentVault.Folders.Add(banking);
        }

        public override void Revert(AppData appData)
        {
            if (_oldVault == null) throw new AssertionException("No old vault");

            appData.CurrentVault.Clone(_oldVault);
        }

#nullable enable
        private Vault? _oldVault;
#nullable restore
    }
}
