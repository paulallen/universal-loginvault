﻿using LoginVault.Models;
using System.Collections.Generic;
using Utils;

namespace LoginVault.Commands
{
#nullable enable

    public class SortFolders : BaseCommand
    {
        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - sort folders");

            foreach (Folder folder in appData.CurrentVault.Folders)
            {
                Logger.D($"Execute() - pre-sort folder #{folder}");
            }

            _oldFolderList = new List<Folder>(appData.CurrentVault.Folders);
            _oldSelectedLocation = appData.SelectedLocation;
            Folder? selectedFolder = appData.CurrentFolder;

            appData.CurrentVault.Folders.Sort();

            foreach (Folder folder in appData.CurrentVault.Folders)
            {
                Logger.D($"Execute() - post-sort folder #{folder}");
            }

            if (selectedFolder != null)
            {
                var location = appData.CurrentVault.FindCardLocationOfFolder(selectedFolder);
                appData.SelectedLocation = location;
            }
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - jumble folders");

            foreach (Folder folder in appData.CurrentVault.Folders)
            {
                Logger.D($"Revert() - pre-revert folder #{folder}");
            }

            for (var n = 0; n < _oldFolderList.Count; ++n)
            {
                appData.CurrentVault.Folders[n] = _oldFolderList[n];
            }

            foreach (Folder folder in appData.CurrentVault.Folders)
            {
                Logger.D($"Revert() - post-revert folder #{folder}");
            }

            appData.SelectedLocation = _oldSelectedLocation;
        }

        private List<Folder> _oldFolderList = new List<Folder>();
#nullable enable
        private CardLocation? _oldSelectedLocation;
#nullable restore
    }
}
