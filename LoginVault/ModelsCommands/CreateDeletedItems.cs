﻿using LoginVault.Models;
using Utils;

namespace LoginVault.Commands
{
    public class CreateDeletedItems : BaseCommand
    {
        public override void Execute(AppData appData)
        {
            if (appData.CurrentVault.GetBackupsFolder() != null) return;

            Logger.D($"Execute() - create deleted items folder");
            appData.CurrentVault.CreateBackupsFolder();
            _createdDeletedFolder = true;
        }

        public override void Revert(AppData appData)
        {
            if (!_createdDeletedFolder) return;

            Logger.D($"Revert() - remove deleted items folder");
            appData.CurrentVault.RemoveBackupFolder();
        }

        private bool _createdDeletedFolder;
    }
}
