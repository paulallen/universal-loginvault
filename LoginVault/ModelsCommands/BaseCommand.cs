﻿using LoginVault.Models;

namespace LoginVault.Commands
{
    public abstract class BaseCommand
    {
        public abstract void Execute(AppData appData);
        public abstract void Revert(AppData appData);
    }
}
