﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class AddFolder : BaseCommand
    {
        public AddFolder(Folder folder)
        {
            _folder = folder ?? throw new ArgumentNullException(nameof(folder));
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - add folder: {_folder}");

            int folderIndex = appData.CurrentVault.Folders.Count;
            appData.CurrentVault.Folders.Insert(folderIndex, _folder);
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - remove last folder");

            int folderIndex = appData.CurrentVault.Folders.Count - 1;
            appData.CurrentVault.Folders.RemoveAt(folderIndex);
        }

        private readonly Folder _folder;
    }
}
