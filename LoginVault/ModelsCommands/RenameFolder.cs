﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class RenameFolder : BaseCommand
    {
        public RenameFolder(int folderIndex, string name)
        {
            _folderIndex = folderIndex;
            _newName = name ?? throw new ArgumentNullException(nameof(name));
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - change folder #{_folderIndex} name to: {_newName}");

            var folder = appData.CurrentVault.Folders[_folderIndex];
            if (folder == null) throw new AssertionException("No folder");

            Logger.D($"Execute() - folder: {folder}, newName: {_newName}");

            _oldName = folder.Name;
            folder.Name = _newName;
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - change folder #{_folderIndex} name back to: {_oldName}");

            var folder = appData.CurrentVault.Folders[_folderIndex];
            if (folder == null) throw new AssertionException("No folder");

            if (_oldName == null) throw new AssertionException("No old name");

            Logger.D($"Revert() - folder #{folder}");

            folder.Name = _oldName;
        }

        private readonly int _folderIndex;
        private readonly string _newName;
#nullable enable
        private string? _oldName;
#nullable restore
    }
}
