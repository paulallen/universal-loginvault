﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class UpdateCard : BaseCommand
    {
        public UpdateCard(int folderIndex, int cardIndex, Card newValues)
        {
            _folderIndex = folderIndex;
            _cardIndex = cardIndex;
            _newValues = newValues ?? throw new ArgumentNullException(nameof(newValues));
        }

        public override void Execute(AppData appData)
        {
            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            Assert.IsTrue(folder != null, $"Execute() - no folder");
            if (folder == null) return;

            Card card = folder.Cards[_cardIndex];
            Assert.IsTrue(card != null, $"Execute() - no card");
            if (card == null) return;

            Logger.D($"Execute() - update folder: {folder}, card: {card}, values: {_newValues}");

            _oldCardProperties = new Card(card);
            card.Clone(_newValues);
        }

        public override void Revert(AppData appData)
        {
            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            Assert.IsTrue(folder != null, $"Revert() - no folder");
            if (folder == null) return;

            Card card = folder.Cards[_cardIndex];
            Assert.IsTrue(card != null, $"Revert() - no card");
            if (card == null) return;

            Assert.IsTrue(_oldCardProperties != null, $"Revert() - no _savedValues");
            if (_oldCardProperties == null) return;

            Logger.D($"Revert() - folder: {folder}, card: {card}, values: {_oldCardProperties}");

            card.Clone(_oldCardProperties);
        }

        private readonly int _folderIndex;
        private readonly int _cardIndex;
        private readonly Card _newValues;
#nullable enable
        private Card? _oldCardProperties;
#nullable restore
    }
}
