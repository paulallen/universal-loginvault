﻿using LoginVault.Models;
using System.Collections.Generic;
using Utils;

namespace LoginVault.Commands
{
    public class SortCards : BaseCommand
    {
        public SortCards(int folderIndex)
        {
            _folderIndex = folderIndex;
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - in folder #{_folderIndex} sort the cards");

            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            if (folder == null) throw new AssertionException("No folder");

            foreach (Card card in folder.Cards)
            {
                Logger.D($"Execute() - pre-sort card: {card}");
            }

            _oldCardList = new List<Card>(folder.Cards);
            _oldSelectedLocation = appData.SelectedLocation;
            Card? selectedCard = appData.CurrentCard;

            folder.Cards.Sort();

            foreach (Card card in folder.Cards)
            {
                Logger.D($"Execute() - post-sort card: {card}");
            }

            if (selectedCard != null)
            {
                var location = appData.CurrentVault.FindCardLocationOfCard(selectedCard);
                appData.SelectedLocation = location;
            }
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - in folder #{_folderIndex} jumble the cards");

            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            if (folder == null) throw new AssertionException("No folder");

            foreach (Card card in folder.Cards)
            {
                Logger.D($"Revert() - pre-revert card: {card}");
            }

            for (var n = 0; n < _oldCardList.Count; ++n)
            {
                folder.Cards[n] = _oldCardList[n];
            }

            foreach (Card card in folder.Cards)
            {
                Logger.D($"Revert() - post-revert card: {card}");
            }

            appData.SelectedLocation = _oldSelectedLocation;
        }

        private readonly int _folderIndex;
        private List<Card> _oldCardList = new List<Card>();
#nullable enable
        private CardLocation? _oldSelectedLocation;
#nullable restore
    }
}
