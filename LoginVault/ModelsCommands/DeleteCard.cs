﻿using LoginVault.Models;
using Utils;

namespace LoginVault.Commands
{
    public class DeleteCard : BaseCommand
    {
        public DeleteCard(int folderIndex, int cardIndex)
        {
            _folderIndex = folderIndex;
            _cardIndex = cardIndex;
        }

        public override void Execute(AppData appData)
        {
            Folder backupsFolder = appData.CurrentVault.GetBackupsFolder();
            if (backupsFolder == null) throw new AssertionException("No backupsFolder");

            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            if (folder == null) throw new AssertionException("No folder");

            Card card = folder.Cards[_cardIndex];
            if (card == null) throw new AssertionException("No card");

            Logger.D($"Execute() - in folder {folder} remove card {card}");

            _oldCard = card;

            // Delete
            appData.CurrentVault.Folders[_folderIndex].Cards.RemoveAt(_cardIndex);

            // Put it in Backups
            if (folder != backupsFolder)
            {
                backupsFolder.Cards.Add(_oldCard);
            }
        }

        public override void Revert(AppData appData)
        {
            Folder backupsFolder = appData.CurrentVault.GetBackupsFolder();
            if (backupsFolder == null) throw new AssertionException("No backupsFolder");

            // Undelete
            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            if (folder == null) throw new AssertionException("No folder");

            if (_oldCard == null) throw new AssertionException("No old card");

            Logger.D($"Revert() - in folder {folder} return card {_oldCard}");

            folder.Cards.Insert(_cardIndex, _oldCard);

            // Remove from Backups
            if (folder != backupsFolder)
            {
                backupsFolder.Cards.RemoveAt(backupsFolder.Cards.Count - 1);
            }
        }

        private readonly int _folderIndex;
        private readonly int _cardIndex;
#nullable enable
        private Card? _oldCard;
#nullable restore
    }
}
