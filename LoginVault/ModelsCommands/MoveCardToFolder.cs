﻿using LoginVault.Models;
using Utils;

namespace LoginVault.Commands
{
    public class MoveCardToFolder : BaseCommand
    {
        public MoveCardToFolder(CardLocation fromLocation, int folderIndex)
        {
            _fromLocation = fromLocation;
            _toIndex = folderIndex;
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - move card from location: {_fromLocation} to: {_toIndex}");

            Folder fromFolder = appData.CurrentVault.FolderAt(_fromLocation);
            Card card = fromFolder.Cards[_fromLocation.CardIndex];

            fromFolder.Cards.RemoveAt(_fromLocation.CardIndex);

            Folder toFolder = appData.CurrentVault.Folders[_toIndex];
            int cardIndex = toFolder.Cards.Count;
            toFolder.Cards.Insert(cardIndex, card);
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - replace card from location: {_toIndex} to: {_fromLocation}");

            Folder toFolder = appData.CurrentVault.Folders[_toIndex];
            int cardIndex = toFolder.Cards.Count - 1;
            Card card = toFolder.Cards[cardIndex];

            toFolder.Cards.RemoveAt(cardIndex);

            Folder fromFolder = appData.CurrentVault.FolderAt(_fromLocation);
            fromFolder.Cards.Insert(_fromLocation.CardIndex, card);
        }

        private readonly CardLocation _fromLocation;
        private readonly int _toIndex;
    }
}
