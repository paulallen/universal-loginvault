﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class SetVault : BaseCommand
    {
        public SetVault(Vault vault)
        {
            _vault = vault ?? throw new ArgumentNullException(nameof(vault));
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - set vault: ${_vault}");

            _oldVault = appData.CurrentVault;
            appData.CurrentVault = _vault;
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - reset vault");

            if (_oldVault == null) throw new AssertionException("No old vault");

            appData.CurrentVault = _oldVault;
        }

        private readonly Vault _vault;
#nullable enable
        private Vault? _oldVault;
#nullable restore
    }
}
