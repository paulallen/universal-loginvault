﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
#nullable enable

    public class SelectFolder : BaseCommand
    {
        public SelectFolder(Folder folder)
        {
            _folder = folder ?? throw new ArgumentNullException(nameof(folder));
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - select: '{_folder}'");

            _oldSelectedLocation = appData.SelectedLocation;

            int folderIndex = appData.CurrentVault.Folders.IndexOf(_folder);

            appData.SelectedLocation = new CardLocation(folderIndex, CardLocation.None);
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - undo to previous selected location: {_oldSelectedLocation}");

            appData.SelectedLocation = _oldSelectedLocation;
        }

        private readonly Folder _folder;
        private CardLocation? _oldSelectedLocation;
    }
}
