﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
#nullable enable

    public class SelectLocation : BaseCommand
    {
        public SelectLocation(CardLocation location)
        {
            _selectedLocation = location ?? throw new ArgumentNullException(nameof(location));
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - set selected location: {_selectedLocation}");

            _oldSelectedLocation = appData.SelectedLocation;

            appData.SelectedLocation = _selectedLocation;
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - undo to previous selected location: {_oldSelectedLocation}");

            appData.SelectedLocation = _oldSelectedLocation;
        }

        private readonly CardLocation? _selectedLocation;
        private CardLocation? _oldSelectedLocation;
    }
}
