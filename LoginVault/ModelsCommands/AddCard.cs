﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class AddCard : BaseCommand
    {
        public AddCard(int folderIndex, Card card)
        {
            _folderIndex = folderIndex;
            _card = card ?? throw new ArgumentNullException(nameof(card));
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - in folder #{_folderIndex} add card: {_card}");

            if (_folderIndex >= appData.CurrentVault.Folders.Count) return;

            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            Logger.D($"Execute() - folder: {folder}");

            int cardIndex = folder.Cards.Count;
            folder.Cards.Insert(cardIndex, _card);
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - in folder #{_folderIndex} remove last card");

            if (_folderIndex >= appData.CurrentVault.Folders.Count) return;

            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            Logger.D($"Revert() - folder: {folder}");

            int cardIndex = folder.Cards.Count - 1;
            folder.Cards.RemoveAt(cardIndex);
        }

        private readonly int _folderIndex;
        private readonly Card _card;
    }
}
