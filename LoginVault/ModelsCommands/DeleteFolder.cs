﻿using LoginVault.Models;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace LoginVault.Commands
{
    public class DeleteFolder : BaseCommand
    {
        public DeleteFolder(int folderIndex)
        {
            _folderIndex = folderIndex;
            _deletedCardsIndexes = new List<int>();
        }

        public override void Execute(AppData appData)
        {
            Folder backupsFolder = appData.CurrentVault.GetBackupsFolder();
            if (backupsFolder == null) throw new AssertionException("No backupsFolder");

            Folder folder = appData.CurrentVault.Folders[_folderIndex];
            if (folder == null) throw new AssertionException("No folder");

            Logger.D($"Execute() - remove folder {folder}");

            _oldFolder = folder;
            _deletedCardsIndexes.Clear();

            // Delete
            appData.CurrentVault.Folders.RemoveAt(_folderIndex);

            // Put cards in Backups, if we haven't just deleted it.
            if (_oldFolder != backupsFolder)
            {
                foreach (Card card in _oldFolder.Cards)
                {
                    int indexInDeleted = backupsFolder.Cards.Count;
                    Logger.D($"Moving card: {card} to deleted folder as card #{indexInDeleted}");

                    _deletedCardsIndexes.Add(indexInDeleted);
                    backupsFolder.Cards.Insert(indexInDeleted, card);
                }
            }
        }

        public override void Revert(AppData appData)
        {
            Folder backupsFolder = appData.CurrentVault.GetBackupsFolder();
            if (backupsFolder == null) throw new AssertionException("No backupsFolder");

            if (_oldFolder == null) throw new AssertionException("No old folder");

            Logger.D($"Revert() - return folder: {_oldFolder}");

            // Undelete
            appData.CurrentVault.Folders.Insert(_folderIndex, _oldFolder);

            // Remove the cards from Backups
            foreach (int indexInDeleted in _deletedCardsIndexes.Reverse())
            {
                Logger.D($"Removing card #{indexInDeleted} in deleted");
                backupsFolder.Cards.RemoveAt(indexInDeleted);
            }
        }

        private readonly int _folderIndex;
        private readonly IList<int> _deletedCardsIndexes;
#nullable enable
        private Folder? _oldFolder;
#nullable restore
    }
}
