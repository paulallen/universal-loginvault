﻿using LoginVault.Models;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace LoginVault.Commands
{
    public class Sequence : BaseCommand
    {
        public Sequence(params BaseCommand[] commands)
        {
            _commands = new List<BaseCommand>(commands);
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - invoking multiple...");

            foreach (BaseCommand cmd in _commands)
            {
                cmd.Execute(appData);
            }
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - revoking multiple...");

            foreach (BaseCommand cmd in _commands.Reverse())
            {
                cmd.Revert(appData);
            }
        }

        private readonly IList<BaseCommand> _commands;
    }
}
