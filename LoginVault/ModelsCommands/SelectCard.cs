﻿using LoginVault.Models;
using System;
using Utils;

namespace LoginVault.Commands
{
#nullable enable

    public class SelectCard : BaseCommand
    {
        public SelectCard(Card card)
        {
            _card = card ?? throw new ArgumentNullException(nameof(card));
        }

        public override void Execute(AppData appData)
        {
            Logger.D($"Execute() - select card: {_card}");

            _oldSelectedLocation = appData.SelectedLocation;

            var location = appData.CurrentVault.FindCardLocationOfCard(_card);

            appData.SelectedLocation = location;
        }

        public override void Revert(AppData appData)
        {
            Logger.D($"Revert() - to previous selected location: {_oldSelectedLocation}");

            appData.SelectedLocation = _oldSelectedLocation;
        }

        private readonly Card _card;
        private CardLocation? _oldSelectedLocation;
    }
}
