﻿using System;
using Utils;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;

namespace LoginVault.Encryption
{
    public class Encrypter
    {
        public IBuffer Encrypt(string message, string password)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            if (string.IsNullOrEmpty(password)) throw new ArgumentException("Invalid password");

            var padder = new Padder();

            IBuffer ibMsg = CryptographicBuffer.ConvertStringToBinary(message, BinaryStringEncoding.Utf8);
            IBuffer ibPadded = padder.Add(ibMsg, BLOCKSIZE_BYTES);
            IBuffer ibEncrypted = EncryptBuffer(ibPadded, password);


            // Self-test
            string strTest = Decrypt(ibEncrypted, password);
            Assert.IsTrue(strTest == message, "Self-test error - Decrypt() != message");

            return ibEncrypted;
        }

        public string Decrypt(IBuffer cipher, string password)
        {
            if (cipher == null) throw new ArgumentNullException(nameof(cipher));
            if (string.IsNullOrEmpty(password)) throw new ArgumentException("Invalid password");

            var padder = new Padder();

            IBuffer ibDecrypted = DecryptBuffer(cipher, password);
            IBuffer ibStripped = padder.Remove(ibDecrypted, BLOCKSIZE_BYTES);

            string str = CryptographicBuffer.ConvertBinaryToString(BinaryStringEncoding.Utf8, ibStripped);
            return str;
        }


        static public IBuffer EncryptBuffer(IBuffer ibPayload, string password)
        {
            if (ibPayload == null) throw new ArgumentNullException(nameof(ibPayload));
            if (password == null) throw new ArgumentNullException(nameof(password));

            Assert.IsTrue(ibPayload.Length % BLOCKSIZE_BYTES == 0, $"EncryptBuffer() - invalid payload.Length: {ibPayload.Length}");
            Assert.IsTrue(!string.IsNullOrEmpty(password), "EncryptBuffer() - invalid password");


            SymmetricKeyAlgorithmProvider aes = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbc);
            Assert.IsTrue(aes.BlockLength == BLOCKSIZE_BYTES, $"EncryptBuffer() - invalid BLOCKSIZE_BYTES: {aes.BlockLength}");
            Assert.IsTrue(aes.BlockLength == INITIAL_VECTOR_LEN, $"EncryptBuffer() - invalid INITIAL_VECTOR_LEN: {aes.BlockLength}");

            // Salt, InitialVector
            IBuffer ibPayloadSaltA = CryptographicBuffer.GenerateRandom(PAYLOAD_SALT_A_LEN);
            IBuffer ibPasswordSalt = CryptographicBuffer.GenerateRandom(PASSWORD_SALT_LEN);
            IBuffer ibInitialVector = CryptographicBuffer.GenerateRandom(INITIAL_VECTOR_LEN);
            IBuffer ibPayloadSaltB = CryptographicBuffer.GenerateRandom(PAYLOAD_SALT_B_LEN);

#if DEBUG && LOG_DETAIL
            Logger.D($"EncryptBuffer() - ibPayloadSaltA: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltA)}'");
            Logger.D($"EncryptBuffer() - ibPasswordSalt: '{CryptographicBuffer.EncodeToBase64String(ibPasswordSalt)}'");
            Logger.D($"EncryptBuffer() - ibInitialVector: '{CryptographicBuffer.EncodeToBase64String(ibInitialVector)}'");
            Logger.D($"EncryptBuffer() - ibPayloadSaltB: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltB)}'");
#endif

            // Password -> key
            IBuffer ibPassword = CryptographicBuffer.ConvertStringToBinary(password, BinaryStringEncoding.Utf8);
            IBuffer ibSaltedPassword = GenerateSaltedPassword(ibPassword, ibPasswordSalt);
            CryptographicKey key = aes.CreateSymmetricKey(ibSaltedPassword);

#if DEBUG && LOG_DETAIL
            Logger.D($"EncryptBuffer() - ibPassword: '{CryptographicBuffer.EncodeToBase64String(ibPassword)}'");
            Logger.D($"EncryptBuffer() - ibSaltedPassword: '{CryptographicBuffer.EncodeToBase64String(ibSaltedPassword)}'");
#endif

            // Encrypt
            IBuffer ibEncrypted = CryptographicEngine.Encrypt(key, ibPayload, ibInitialVector);

            // Concat
            IBuffer ibAll = AssemblePayload(ibPayloadSaltA, ibPasswordSalt, ibInitialVector, ibEncrypted, ibPayloadSaltB);
            return ibAll;
        }

        static public IBuffer DecryptBuffer(IBuffer ibPayload, string password)
        {
            if (ibPayload == null) throw new ArgumentNullException(nameof(ibPayload));
            if (password == null) throw new ArgumentNullException(nameof(password));

            SymmetricKeyAlgorithmProvider aes = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbc);
            Assert.IsTrue(aes.BlockLength == BLOCKSIZE_BYTES, $"DecryptBuffer() - invalid BlockLength: {aes.BlockLength}");
            Assert.IsTrue(!string.IsNullOrEmpty(password), "DecryptBuffer() - invalid password");

            // Split
            DisassemblePayload(ibPayload, out IBuffer ibPayloadSaltA, out IBuffer ibPasswordSalt, out IBuffer ibInitialVector, out IBuffer ibPadded, out IBuffer ibPayloadSaltB);

#if DEBUG && LOG_DETAIL
            Logger.D($"DecryptBuffer() - ibPayloadSaltA: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltA)}'");
            Logger.D($"DecryptBuffer() - ibPasswordSalt: '{CryptographicBuffer.EncodeToBase64String(ibPasswordSalt)}'");
            Logger.D($"DecryptBuffer() - ibInitialVector: '{CryptographicBuffer.EncodeToBase64String(ibInitialVector)}'");
            Logger.D($"DecryptBuffer() - ibPayloadSaltB: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltB)}'");
#endif

            // Password -> key
            IBuffer ibPassword = CryptographicBuffer.ConvertStringToBinary(password, BinaryStringEncoding.Utf8);
            IBuffer ibSaltedPassword = GenerateSaltedPassword(ibPassword, ibPasswordSalt);
            CryptographicKey key = aes.CreateSymmetricKey(ibSaltedPassword);


#if DEBUG && LOG_DETAIL
            Logger.D($"DecryptBuffer() - ibPassword: '{CryptographicBuffer.EncodeToBase64String(ibPassword)}'");
            Logger.D($"DecryptBuffer() - ibSaltedPassword: '{CryptographicBuffer.EncodeToBase64String(ibSaltedPassword)}'");
#endif

            // Decrypt
            IBuffer ibDecrypted = CryptographicEngine.Decrypt(key, ibPadded, ibInitialVector);
            return ibDecrypted;
        }


        static public IBuffer AssemblePayload(IBuffer ibPayloadSaltA, IBuffer ibPasswordSalt, IBuffer ibInitialVector, IBuffer ibMsg, IBuffer ibPayloadSaltB)
        {
            if (ibPayloadSaltA == null) throw new ArgumentNullException(nameof(ibPayloadSaltA));
            if (ibPasswordSalt == null) throw new ArgumentNullException(nameof(ibPasswordSalt));
            if (ibInitialVector == null) throw new ArgumentNullException(nameof(ibInitialVector));
            if (ibMsg == null) throw new ArgumentNullException(nameof(ibMsg));
            if (ibPayloadSaltB == null) throw new ArgumentNullException(nameof(ibPayloadSaltB));

            Assert.IsTrue(ibPayloadSaltA.Length == PAYLOAD_SALT_A_LEN, "AssemblePayload() - invalid saltA length");
            Assert.IsTrue(ibPayloadSaltB.Length == PAYLOAD_SALT_B_LEN, "AssemblePayload() - invalid saltB length");
            Assert.IsTrue(ibPasswordSalt.Length == PASSWORD_SALT_LEN, "AssemblePayload() - invalid salt length");
            Assert.IsTrue(ibInitialVector.Length == BLOCKSIZE_BYTES, "AssemblePayload() - invalid initial vector length");


            using DataWriter dw = new DataWriter();
            dw.WriteBuffer(ibPayloadSaltA);
            dw.WriteBuffer(ibPasswordSalt);
            dw.WriteBuffer(ibInitialVector);
            dw.WriteBuffer(ibMsg);
            dw.WriteBuffer(ibPayloadSaltB);

            IBuffer ibPayload = dw.DetachBuffer();

            // Self-test
            uint nCheck = ibPayloadSaltA.Length + ibPasswordSalt.Length + ibInitialVector.Length + ibMsg.Length + ibPayloadSaltB.Length;
            Assert.IsTrue(nCheck == ibPayload.Length, $"AssemblePayload() - assembled payload length: {ibPayload.Length} is not sum: {nCheck}");

            return ibPayload;
        }

        static public void DisassemblePayload(IBuffer ibPayload, out IBuffer ibPayloadSaltA, out IBuffer ibPasswordSalt, out IBuffer ibInitialVector, out IBuffer ibMsg, out IBuffer ibPayloadSaltB)
        {
            if (ibPayload == null) throw new ArgumentNullException(nameof(ibPayload));

            Assert.IsTrue(ibPayload.Length > PAYLOAD_SALT_A_LEN + PAYLOAD_SALT_B_LEN + PASSWORD_SALT_LEN + INITIAL_VECTOR_LEN + 1, $"DisassemblePayload() - invalid payload length: {ibPayload.Length}");


            uint nMsgLen = ibPayload.Length - PAYLOAD_SALT_A_LEN - PAYLOAD_SALT_B_LEN - PASSWORD_SALT_LEN - INITIAL_VECTOR_LEN;
            uint uStart = 0;

            ReadBuffer(ibPayload, out ibPayloadSaltA, ref uStart, PAYLOAD_SALT_A_LEN);
            ReadBuffer(ibPayload, out ibPasswordSalt, ref uStart, PASSWORD_SALT_LEN);
            ReadBuffer(ibPayload, out ibInitialVector, ref uStart, INITIAL_VECTOR_LEN);
            ReadBuffer(ibPayload, out ibMsg, ref uStart, nMsgLen);
            ReadBuffer(ibPayload, out ibPayloadSaltB, ref uStart, PAYLOAD_SALT_B_LEN);


            // Self-test
            uint uCheck = ibPayloadSaltA.Length + ibPasswordSalt.Length + ibInitialVector.Length + ibMsg.Length + ibPayloadSaltB.Length;
            Assert.IsTrue(uCheck == ibPayload.Length, $"Self-test error - sum of disassembled parts: {uCheck} is not total length: {ibPayload.Length}");
        }


        static public void ReadBuffer(IBuffer ibFrom, out IBuffer ibTo, ref uint uStart, uint uLength)
        {
            if (ibFrom == null) throw new ArgumentNullException(nameof(ibFrom));

            Assert.IsTrue(uStart + uLength <= ibFrom.Length, "ReadBuffer() - invalid uStart, uLength parameter/s");


            using (DataWriter dw = new DataWriter())
            {
                dw.WriteBuffer(ibFrom, uStart, uLength);
                ibTo = dw.DetachBuffer();
                uStart += uLength;
            }


            Assert.IsTrue(ibTo.Length == uLength, $"ReadBuffer() - unexpected output buffer length: {ibTo.Length}, requested: {uLength}");
        }

        static public IBuffer GenerateSaltedPassword(IBuffer ibPassword, IBuffer ibPasswordSalt)
        {
            if (ibPassword == null) throw new ArgumentNullException(nameof(ibPassword));
            if (ibPasswordSalt == null) throw new ArgumentNullException(nameof(ibPasswordSalt));

            Assert.IsTrue(ibPassword.Length > 0, "GenerateSaltedPassword() - invalid password parameter");
            Assert.IsTrue(ibPasswordSalt.Length > 0, "GenerateSaltedPassword() - invalid salt parameter");

            using DataWriter dw = new DataWriter();
            dw.WriteBuffer(ibPassword);
            dw.WriteBuffer(ibPasswordSalt);

            IBuffer ibSalted = dw.DetachBuffer();

            Assert.IsTrue(ibSalted.Length == ibPasswordSalt.Length + ibPassword.Length, $"GenerateSaltedPassword() - salted password length: {ibSalted.Length} is not sum: {ibPasswordSalt.Length + ibPassword.Length}");
            return ibSalted;
        }


        private const int BLOCKSIZE_BYTES = 16;
        private const int KEYSIZE_BYTES = 32;

        private const uint INITIAL_VECTOR_LEN = BLOCKSIZE_BYTES;
        private const uint PASSWORD_SALT_LEN = KEYSIZE_BYTES;
        private const uint PAYLOAD_SALT_A_LEN = 13;
        private const uint PAYLOAD_SALT_B_LEN = 7;
    }
}
