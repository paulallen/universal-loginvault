﻿using System;
using System.Globalization;
using Utils;
using Windows.Storage.Streams;

namespace LoginVault.Encryption
{
    public class Padder
    {
        public IBuffer Add(IBuffer buffer, byte blockSizeBytes)
        {
            if (buffer == null) throw new ArgumentNullException(nameof(buffer));

            uint uMsgLength = buffer.Length;
            uint uPaddedSize = PaddedBufferLength(buffer.Length, blockSizeBytes);
            byte bPaddingByte = (byte)(uPaddedSize - uMsgLength);

            Assert.IsTrue(bPaddingByte <= blockSizeBytes, string.Format(CultureInfo.InvariantCulture, $"Invalid padding count: {bPaddingByte}"));

#if DEBUG && LOG_DETAIL
            Logger.D($"CreatePaddedBuffer() - padding byte: {bPaddingByte}");
#endif

            using (DataWriter writer = new DataWriter())
            {
                writer.WriteBuffer(buffer);

                for (uint u = uMsgLength; u < uPaddedSize; ++u)
                {
                    writer.WriteByte(bPaddingByte);
                }

                IBuffer paddedBuffer = writer.DetachBuffer();

                return paddedBuffer;
            }
        }

        public IBuffer Remove(IBuffer paddedBuffer, byte blockSizeBytes)
        {
            if (paddedBuffer == null) throw new ArgumentNullException(nameof(paddedBuffer));

            uint uPaddedLength = paddedBuffer.Length;

            DataReader reader = DataReader.FromBuffer(paddedBuffer);
            byte[] abPadded = new byte[paddedBuffer.Length];
            reader.ReadBytes(abPadded);
            Assert.IsTrue(abPadded.Length == paddedBuffer.Length, "CreateStrippedBuffer() - read wrong length from buffer");

            byte bPaddingByte = abPadded[uPaddedLength - 1];
            uint uStrippedLength = (uint)(abPadded.Length - bPaddingByte);

#if DEBUG && LOG_DETAIL
            Logger.D($"CreateStrippedBuffer() - padding byte: {bPaddingByte}");
#endif

            Assert.IsTrue(bPaddingByte <= blockSizeBytes, string.Format(CultureInfo.InvariantCulture, $"Invalid padding count: {bPaddingByte}"));
            for (uint uCheck = uStrippedLength; uCheck < uPaddedLength; ++uCheck)
            {
                Assert.IsTrue(abPadded[uCheck] == bPaddingByte, string.Format(CultureInfo.InvariantCulture, $"Invalid padding byte :{abPadded[uCheck]}"));
            }

            using (DataWriter writer = new DataWriter())
            {
                writer.WriteBuffer(paddedBuffer, 0, uStrippedLength);
                IBuffer ibStripped = writer.DetachBuffer();
                return ibStripped;
            }
        }

        static public uint PaddedBufferLength(uint messageLength, byte blockSizeBytes)
        {
            uint uPaddedLength = messageLength + 1;
            if (uPaddedLength % blockSizeBytes != 0)
            {
                uPaddedLength = ((uPaddedLength + blockSizeBytes) / blockSizeBytes) * blockSizeBytes;
            }
            return uPaddedLength;
        }
    }
}
