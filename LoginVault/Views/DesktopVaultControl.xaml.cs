﻿using LoginVault.Models;
using LoginVault.ViewModels;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Views
{
    public sealed partial class DesktopVaultControl
    {
        public DesktopVaultControl()
        {
            InitializeComponent();
        }

        private void Folders_ItemClick(object sender, Windows.UI.Xaml.Controls.ItemClickEventArgs e)
        {
            if (!(DataContext is AppVm appVm)) return;
            if (e == null) return;
            if (!(e.ClickedItem is Folder folder)) return;

            appVm.FolderClick(folder);
        }

        private void Cards_ItemClick(object sender, Windows.UI.Xaml.Controls.ItemClickEventArgs e)
        {
            if (!(DataContext is AppVm appVm)) return;
            if (e == null) return;
            if (!(e.ClickedItem is Card card)) return;

            appVm.CardClick(card);
        }

        private void Grid_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (!(DataContext is AppVm appVm)) return;
            if (e == null) return;

            var ctrl = Window.Current.CoreWindow.GetKeyState(VirtualKey.Control);

            if (ctrl.HasFlag(CoreVirtualKeyStates.Down) && e.Key == VirtualKey.Z)
            {
                appVm.CmdUndo.Execute(null);
            }
            else if (ctrl.HasFlag(CoreVirtualKeyStates.Down) && e.Key == VirtualKey.Y)
            {
                appVm.CmdRedo.Execute(null);
            }
        }

        private void DragItems_Starting(object sender, DragItemsStartingEventArgs e)
        {
            if (!(DataContext is AppVm appVm)) return;
            if (e == null) return;
            if (e.Items == null || e.Items.Count == 0) return;
            if (!(e.Items[0] is Card)) return;

            appVm.DragItemsStarting(e);
        }

        private void Drag_Over(object sender, DragEventArgs e)
        {
            if (!(DataContext is AppVm)) return;
            if (e == null) return;

            AppVm.DragOver(e);
        }

        private void Drag_Drop(object sender, DragEventArgs e)
        {
            if (!(DataContext is AppVm appVm)) return;
            if (e == null) return;

            appVm.Drop(sender, e);
        }
    }
}
