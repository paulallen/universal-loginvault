﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace LoginVault.Views.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public bool VisibleIfFalse { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is bool))
            {
                return DependencyProperty.UnsetValue;
            }

            return ((bool)value ^ VisibleIfFalse) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (!(value is Visibility))
            {
                return DependencyProperty.UnsetValue;
            }

            return (((Visibility)value) == Visibility.Visible) ^ VisibleIfFalse;
        }
    }
}
