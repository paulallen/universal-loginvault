﻿using LoginVault.Models;
using Windows.UI.Xaml;

namespace Views
{
    public sealed partial class CardControl
    {
        public Card Card
        {
            get { return (Card)GetValue(CardProperty); }
            set { SetValue(CardProperty, value); }
        }

        public static readonly DependencyProperty CardProperty =
            DependencyProperty.Register("Card", typeof(Card), typeof(CardControl), new PropertyMetadata(0));


        public bool IsEditingCard
        {
            get { return (bool)GetValue(IsEditingCardProperty); }
            set { SetValue(IsEditingCardProperty, value); }
        }

        public static readonly DependencyProperty IsEditingCardProperty =
            DependencyProperty.Register("IsEditingCard", typeof(bool), typeof(CardControl), new PropertyMetadata(false));


        public CardControl()
        {
            InitializeComponent();
        }
    }
}
