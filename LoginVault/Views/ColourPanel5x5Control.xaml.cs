﻿using LoginVault.Models.Settings;
using LoginVault.Services;
using LoginVault.ViewModels;
using Utils;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace Views
{
    public sealed partial class ColourPanel5x5Control
    {
        // nb. 0, 2^5-1, 2^6-1, 2^7-1, 2^8-1
        private readonly byte[] _colourValues = { 0, 63, 127, 191, 255 };

        private byte _sliderValue;

        public ColourPanel5x5Control()
        {
            InitializeComponent();

            Loaded += ColourPanelLoaded;
        }

        public void ColourPanelLoaded(object sender, RoutedEventArgs e)
        {
            _sliderValue = 0;

            Fill();
        }

        public void Clear()
        {
            grid.Children.Clear();
        }

        public void Fill()
        {
            byte a = 255;

            Thickness margin = new Thickness(1, 1, 0, 0);

            // Hold red
            byte r = _sliderValue;

            // Increasing blue down
            int row = 0;
            foreach (byte b in _colourValues)
            {
                // Increasing green across
                int column = 0;
                foreach (byte g in _colourValues)
                {
                    Rectangle rect = new Rectangle
                    {
                        Height = 40,
                        Width = 40,
                        Margin = margin,
                        StrokeThickness = 0
                    };
                    rect.Tapped += Rectangle_Tapped;

                    Grid.SetRow(rect, row);
                    Grid.SetColumn(rect, column);

                    Color c = Color.FromArgb(a, r, g, b);
                    Brush brush = new SolidColorBrush(c);
                    rect.Fill = brush;

                    grid.Children.Add(rect);

                    ++column;
                }
                ++row;
            }
        }

        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (e == null) return;

            int n = (int)e.NewValue;
            Assert.IsTrue(n >= 0 && n <= 4, "Invalid slider value");

            _sliderValue = _colourValues[n];

            Clear();
            Fill();
        }

        private void Rectangle_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (!(sender is Rectangle rect)) return;
            if (!(rect.Fill is SolidColorBrush fill)) return;

            Color colour = fill.Color;

            // It's a bit hacky doing this here!
            SaveThemeColour(colour);
            ((AppVm)DataContext).SetThemeColour(colour);
        }

        static private void SaveThemeColour(Color colour)
        {
            ThemeColourSetting settings = new ThemeColourSetting();

            settings.Set(ColourHelpers.ToArgb(colour));
        }
    }
}
