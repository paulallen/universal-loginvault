﻿using LoginVault.Models;
using LoginVault.ViewModels;

namespace Views
{
    public sealed partial class PhoneVaultControl
    {
        public PhoneVaultControl()
        {
            InitializeComponent();
        }

        private void Cards_ItemClick(object sender, Windows.UI.Xaml.Controls.ItemClickEventArgs e)
        {
            if (!(DataContext is AppVm appVm)) return;
            if (e == null) return;
            if (!(e.ClickedItem is Card card)) return;

            appVm.CardClick(card);
            appVm.PhoneOnlyNavigateToCard();
        }
    }
}
