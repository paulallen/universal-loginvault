﻿namespace LoginVault.Models
{
    public interface ISortable
    {
        public string SortField { get; }
    }
}
