﻿namespace LoginVault.Models
{
    public class CardLocation
    {
        public const int None = -1;

        public int FolderIndex { get; }
        public int CardIndex { get; }

        public CardLocation()
        {
            FolderIndex = None;
            CardIndex = None;
        }

        public CardLocation(int folderIndex, int cardIndex = None)
        {
            FolderIndex = folderIndex;
            CardIndex = cardIndex;
        }

        public override string ToString() =>
            $"CardLocation(FolderIndex: {FolderIndex}, CardIndex: {CardIndex})";
    }
}
