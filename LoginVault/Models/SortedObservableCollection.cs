﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Utils;

namespace LoginVault.Models
{
    public static class SortedObservableCollection
    {
        public static void Sort<T>(this ObservableCollection<T> collection) where T : ISortable
        {
            if (collection == null) return;

            List<T> sorted = collection.OrderBy(x => x.SortField).ToList();
            Assert.IsTrue(sorted.Count == collection.Count, "Sorted collection is not same size");

            // Move(old, i) implementation seems to get missed by Xamarin sometimes
            collection.Clear();
            for (var i = 0; i < sorted.Count; ++i)
            {
                collection.Add(sorted[i]);
            }
        }
    }
}
