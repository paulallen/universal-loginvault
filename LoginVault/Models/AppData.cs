﻿
using LoginVault.Mvvm;
using Utils;

namespace LoginVault.Models
{
#nullable enable

    public class AppData : Observable
    {
        private Vault _currentVault = new Vault();
        public Vault CurrentVault
        {
            get { return _currentVault; }
            set { SetField(ref _currentVault, value); }
        }

        private Folder? _currentFolder;
        public Folder? CurrentFolder
        {
            get { return _currentFolder; }
            set { SetField(ref _currentFolder, value); }
        }

        private Card? _currentCard;
        public Card? CurrentCard
        {
            get { return _currentCard; }
            set { SetField(ref _currentCard, value); }
        }

        private CardLocation? _selectedLocation;
        public CardLocation? SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                Logger.D($"SelectedLocation: f# {value?.FolderIndex}, c# {value?.CardIndex}");

                _selectedLocation = value;

                if (_selectedLocation == null || _selectedLocation.FolderIndex == CardLocation.None)
                {
                    CurrentFolder = null;
                    CurrentCard = null;
                }
                else if (_selectedLocation.CardIndex == CardLocation.None)
                {
                    CurrentFolder = CurrentVault.FolderAt(_selectedLocation);
                    CurrentCard = null;
                }
                else
                {
                    CurrentFolder = CurrentVault.FolderAt(_selectedLocation);
                    CurrentCard = CurrentVault.CardAt(_selectedLocation);
                }
            }
        }
    }
}
