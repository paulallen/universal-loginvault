﻿using LoginVault.Mvvm;
using System;

namespace LoginVault.Models
{
    public class Card : ChangeTracked, ISortable
    {
        public string Url
        {
            get { return _url; }
            set { SetFieldTracked(ref _url, value); }
        }

        public string Username
        {
            get { return _username; }
            set { SetFieldTracked(ref _username, value); }
        }

        public string Password
        {
            get { return _password; }
            set { SetFieldTracked(ref _password, value); }
        }

        public string Other
        {
            get { return _other; }
            set { SetFieldTracked(ref _other, value); }
        }

        public Card()
            : base()
        {
            _url = "";
            _username = "";
            _password = "";
            _other = "";
        }

        public Card(Card source)
            : this()
        {
            if (source != null)
            {
                Clone(source);
            }
        }

        public Card(string url, string username, string password, string other)
            : this()
        {
            Url = url;
            Username = username;
            Password = password;
            Other = other;
            IsChanged = false;
        }

        public string SortField => Url.ToUpperInvariant();

        public void Clone(Card card)
        {
            if (card == null) throw new ArgumentNullException(nameof(card));

            Url = card.Url;
            Username = card.Username;
            Password = card.Password;
            Other = card.Other;
            IsChanged = card.IsChanged;
        }

        public bool IsSameAs(Card other)
        {
            return other != null
                && Url == other.Url
                && Username == other.Username
                && Password == other.Password
                && Other == other.Other;
        }


        public override string ToString() =>
            $"Card(Hash={GetHashCode()}, Url: {Url}, Username: {Username})";

        private string _url;
        private string _username;
        private string _password;
        private string _other;
    }
}
