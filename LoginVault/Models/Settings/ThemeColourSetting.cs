﻿namespace LoginVault.Models.Settings
{
    public class ThemeColourSetting : LocalStringSetting
    {
        public string Get(string defaultColour)
        {
            return GetSetting(THEME_COLOUR_KEY, defaultColour);
        }

        public void Set(string colour)
        {
            SetSetting(THEME_COLOUR_KEY, colour);
        }

        public void Clear()
        {
            ClearSetting(THEME_COLOUR_KEY);
        }

        private const string THEME_COLOUR_KEY = "ThemeColour";
    }
}
