﻿namespace LoginVault.Models.Settings
{
    public interface IStringSetting
    {
        string GetSetting(string name, string defaultValue);
        void SetSetting(string name, string value);
        void ClearSetting(string name);
    }
}
