﻿using Utils;
using Windows.Storage;

namespace LoginVault.Models.Settings
{
    public class LocalStringSetting : IStringSetting
    {
        public string GetSetting(string name, string defaultValue)
        {
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey(name))
            {
                object setting = ApplicationData.Current.LocalSettings.Values[name];
                if (setting is string value)
                {
                    Logger.D($"LocalStringSetting.Get(name: '{name}') - return value: '{value}'");
                    return value;
                }
            }

            Logger.D($"LocalStringSetting.Get(name: '{name}') - return default: '{defaultValue}'");
            return defaultValue;
        }

        public void SetSetting(string name, string value)
        {
            Logger.D($"LocalStringSetting.Set(name: '{name}', value: '{value}')");

            ApplicationData.Current.LocalSettings.Values[name] = value;
        }

        public void ClearSetting(string name)
        {
            Logger.D($"LocalStringSetting.Clear(name: '{name}')");

            ApplicationData.Current.LocalSettings.Values.Remove(name);
        }
    }
}
