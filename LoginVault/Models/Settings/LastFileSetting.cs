﻿namespace LoginVault.Models.Settings
{
    public class LastFileSetting : LocalStringSetting
    {
        public string Get(string defaultPath)
        {
            return GetSetting(LAST_FILE_KEY, defaultPath);
        }

        public void Set(string path)
        {
            SetSetting(LAST_FILE_KEY, path);
        }

        public void Clear()
        {
            ClearSetting(LAST_FILE_KEY);
        }

        private const string LAST_FILE_KEY = "LastFile";
    }
}
