﻿using System;
using Utils;

namespace LoginVault.Models
{
    public static class CardLocationExtensions
    {
        public static CardLocation FindCardLocationOfFolder(this Vault vault, Folder folder)
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));
            if (folder == null) throw new ArgumentNullException(nameof(folder));

            var folderIndex = vault.Folders.IndexOf(folder);
            return new CardLocation(folderIndex);
        }

        public static CardLocation FindCardLocationOfCard(this Vault vault, Card card)
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));
            if (card == null) throw new ArgumentNullException(nameof(card));

            for (int nFolder = 0; nFolder < vault.Folders.Count; ++nFolder)
            {
                Folder folder = vault.Folders[nFolder];
                int nCard = folder.Cards.IndexOf(card);
                if (nCard >= 0)
                {
                    return new CardLocation(nFolder, nCard);
                }
            }

            Logger.W($"Card: {card} not found");
            return new CardLocation(CardLocation.None, CardLocation.None);
        }

        public static Folder FolderAt(this Vault vault, CardLocation location)
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));
            if (location == null) throw new ArgumentNullException(nameof(location));

            if (location.FolderIndex < 0 || location.FolderIndex >= vault.Folders.Count) throw new ArgumentException($"Invalid FolderIndex: {location.FolderIndex}");

            return vault.Folders[location.FolderIndex];
        }

        public static Card CardAt(this Vault vault, CardLocation location)
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));
            if (location == null) throw new ArgumentNullException(nameof(location));

            if (location.FolderIndex < 0 || location.FolderIndex >= vault.Folders.Count) throw new ArgumentException($"Invalid FolderIndex: {location.FolderIndex}");
            if (location.CardIndex < 0 || location.CardIndex >= vault.Folders[location.FolderIndex].Cards.Count) throw new ArgumentException($"Invalid CardIndex: {location.CardIndex}");

            return vault.Folders[location.FolderIndex].Cards[location.CardIndex];
        }
    }
}
