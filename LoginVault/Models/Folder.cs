﻿using LoginVault.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace LoginVault.Models
{
    public class Folder : ChangeTracked, ISortable
    {
        public ObservableCollection<Card> Cards { get; }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetFieldTracked(ref _name, value); }
        }

        public Folder()
            : base()
        {
            Cards = new ObservableCollection<Card>();
            Cards.CollectionChanged += Cards_OnCollectionChanged;

            _name = "";
        }

        public Folder(Folder source)
            : this()
        {
            if (source != null)
            {
                Clone(source);
            }
        }

        public void Clone(Folder folder)
        {
            if (folder == null) throw new ArgumentNullException(nameof(folder));

            Cards.Clear();
            foreach (Card c in folder.Cards)
            {
                Card card = new Card();
                card.Clone(c);

                Cards.Add(card);
            }

            Name = folder.Name;
            IsChanged = folder.IsChanged;
        }

        public bool IsSameAs(Folder other)
        {
            if (other == null
                || Name != other.Name
                || Cards.Count != other.Cards.Count)
            {
                return false;
            }

            for (int n = 0; n < Cards.Count; ++n)
            {
                if (!Cards[n].IsSameAs(other.Cards[n]))
                {
                    return false;
                }
            }

            return true;
        }

        public string SortField => Name.ToUpperInvariant();

        public override string ToString() =>
            $"Folder(Hash={GetHashCode()}, Name={Name})";

        public void NotChanged()
        {
            foreach (Card card in Cards)
            {
                card.IsChanged = false;
            }

            IsChanged = false;
        }

        private void Cards_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (Card card in e.OldItems)
                {
                    card.PropertyChanged -= Card_OnPropertyChanged;
                }
            }

            if (e.NewItems != null)
            {
                foreach (Card card in e.NewItems)
                {
                    card.PropertyChanged += Card_OnPropertyChanged;
                }
            }

            IsChanged = true;
        }

        private void Card_OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsChanged = true;
        }
    }
}
