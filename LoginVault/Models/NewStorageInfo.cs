﻿namespace LoginVault.Models
{
    public class NewStorageInfo
    {
        public string Path { get; set; } = "";
        public string Password { get; set; } = "";
        public bool IsReadOnly { get; set; }
        public Vault Contents { get; set; } = new Vault();
    }
}
