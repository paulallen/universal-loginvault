﻿using LoginVault.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace LoginVault.Models
{
    public class Vault : ChangeTracked
    {
        public ObservableCollection<Folder> Folders { get; }

        private string _name = "";
        public string Name
        {
            get { return _name; }
            set { SetFieldTracked(ref _name, value); }
        }

        public Vault()
            : base()
        {
            Folders = new ObservableCollection<Folder>();
            Folders.CollectionChanged += Folders_OnCollectionChanged;
        }

        public Vault(Vault source)
            : this()
        {
            if (source != null)
            {
                Clone(source);
            }
        }

        public Vault(string name)
            : this()
        {
            Name = name;
        }

        public void Clear()
        {
            Folders.Clear();
            IsChanged = false;
        }

        public void Clone(Vault vault)
        {
            if (vault == null) throw new ArgumentNullException(nameof(vault));

            Name = vault.Name;

            Folders.Clear();
            foreach (Folder f in vault.Folders)
            {
                Folder folder = new Folder();
                folder.Clone(f);

                Folders.Add(folder);
            }

            IsChanged = vault.IsChanged;
        }

        public bool IsSameAs(Vault other)
        {
            if (other == null
                || Name != other.Name
                || Folders.Count != other.Folders.Count)
            {
                return false;
            }

            for (int n = 0; n < Folders.Count; ++n)
            {
                if (!Folders[n].IsSameAs(other.Folders[n]))
                {
                    return false;
                }
            }

            return true;
        }

        public override string ToString() =>
            $"Vault(Hash={GetHashCode()}, Name={Name})";

        public void NotChanged()
        {
            foreach (Folder folder in Folders)
            {
                folder.NotChanged();
            }

            IsChanged = false;
        }

        private void Folders_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (Folder folder in e.OldItems)
                {
                    folder.PropertyChanged -= Folder_OnPropertyChanged;
                }
            }

            if (e.NewItems != null)
            {
                foreach (Folder folder in e.NewItems)
                {
                    folder.PropertyChanged += Folder_OnPropertyChanged;
                }
            }

            IsChanged = true;
        }

        private void Folder_OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsChanged = true;
        }


        private const string DELETED_CARDS = "Deleted Cards";

        public Folder GetBackupsFolder()
        {
            return Folders.FirstOrDefault(f => f.Name == DELETED_CARDS);
        }

        public Folder CreateBackupsFolder()
        {
            Folder folder = new Folder { Name = DELETED_CARDS };
            Folders.Add(folder);
            return folder;
        }

        public void RemoveBackupFolder()
        {
            Folder folder = GetBackupsFolder();
            Folders.Remove(folder);
        }
    }
}
