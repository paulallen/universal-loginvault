﻿namespace LoginVault.DtosV2
{
    public class DtoCard
    {
        public string Url { get; set; } = "";
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
        public string Other { get; set; } = "";
    }
}
