﻿using System.Collections.Generic;

namespace LoginVault.DtosV2
{
    public class DtoVault
    {
        public List<DtoFolder> Folders { get; } = new List<DtoFolder>();
    }
}
