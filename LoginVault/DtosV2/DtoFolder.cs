﻿using System.Collections.Generic;

namespace LoginVault.DtosV2
{
    public class DtoFolder
    {
        public string Name { get; set; } = "";
        public List<DtoCard> Cards { get; } = new List<DtoCard>();
    }
}
