﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LoginVault.DtosV2
{
    internal class DtoSerializer
    {
        private class LowercaseContractResolver : DefaultContractResolver
        {
            protected override string ResolvePropertyName(string propertyName)
            {
                return propertyName.ToLowerInvariant();
            }
        }

        public string Serialize(DtoVault vault)
        {
            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = new LowercaseContractResolver();
            return JsonConvert.SerializeObject(vault, formatting: Formatting.None, settings: serializerSettings);
        }

        public DtoVault Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<DtoVault>(json);
        }
    }
}
