﻿using LoginVault.ViewModels;

namespace LoginVault
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void OnTimer()
        {
            if (!(DataContext is AppVm appVm)) return;

            _ = appVm.OnTimer();
        }
    }
}
