﻿namespace LoginVault.Services
{
    public interface IPasswordHelper
    {
        string Rate(string password);
        string CheckPasswords(string password, string repeatPassword);
    }
}
