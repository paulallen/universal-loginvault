﻿using System;
using System.Threading.Tasks;
using Utils;
using Windows.UI.Popups;

namespace LoginVault.Services
{
    public class PopupService : IPopupService
    {
        public async Task<bool> ShowOfferExampleDialog()
        {
            Logger.D("ShowOfferExampleDialog()");

            MessageDialog dialog = new MessageDialog("Do you want an example vault to get you started?");
            dialog.Commands.Add(new UICommand { Label = "Yes", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "No", Id = 1 });

            dialog.DefaultCommandIndex = 0;
            dialog.CancelCommandIndex = 1;

            IUICommand command = await dialog.ShowAsync();
            return (int)command.Id == 0;
        }

        public async Task<bool> ShowSaveChangesDialog()
        {
            Logger.D("ShowSaveChangesDialog()");

            MessageDialog dialog = new MessageDialog("You have unsaved changes.  Do you want to save them?");
            dialog.Commands.Add(new UICommand { Label = "Save", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Discard", Id = 1 });

            dialog.DefaultCommandIndex = 0;
            dialog.CancelCommandIndex = 1;

            IUICommand command = await dialog.ShowAsync();
            return (int)command.Id == 0;
        }

        public async Task ShowOpenFailMessage()
        {
            Logger.D("ShowOpenFailMessage()");

            MessageDialog dialog = new MessageDialog("Unable to open file.  Ensure that this is a valid Login Vault file and that your password is correct.");
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });

            await dialog.ShowAsync();
        }

        public async Task ShowSaveFailMessage()
        {
            Logger.D("ShowSaveFailMessage()");

            MessageDialog dialog = new MessageDialog("Unable to save file.  Try another location.");
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });

            await dialog.ShowAsync();
        }

        public async Task ShowUnhandledException(Exception ex)
        {
            if (ex == null) return;

            Logger.D($"ShowUnhandledException(ex: {ex})");

            MessageDialog dialog = new MessageDialog($"That didn't go well.  Sorry.\r\n\r\n{ex}");
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });

            await dialog.ShowAsync();
        }
    }
}
