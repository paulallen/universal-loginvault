﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Models.Settings;
using LoginVault.Serialization;
using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using Utils;
using Windows.Storage;

namespace LoginVault.Services
{
#nullable enable

    public class RecoveryFileService : IRecoveryFileService
    {
        private const string RECOVERY_FILE_NAME = "Recovery.lv1";

        private readonly AppData _appData;
        private readonly ILocalFolderService _localFolderService;
        private readonly IDialogService _dialogService;
        private readonly CommandService _commandService;
        private readonly StorageService _storageService;
        private readonly IModelLoader _loader;

        public NewStorageInfo? NewRecoveryInfo { get; private set; }

        public bool IsSameAsLast => NewRecoveryInfo != null && _appData.CurrentVault != null && _appData.CurrentVault.IsSameAs(NewRecoveryInfo.Contents);

        public RecoveryFileService(AppData appData, CommandService commandService, ILocalFolderService localFolderService, StorageService storageService, IDialogService dialogService, IModelLoader modelLoader)
        {
            _appData = appData ?? throw new ArgumentNullException(nameof(appData));
            _commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));
            _localFolderService = localFolderService ?? throw new ArgumentNullException(nameof(localFolderService));
            _storageService = storageService ?? throw new ArgumentNullException(nameof(storageService));
            _dialogService = dialogService ?? throw new ArgumentNullException(nameof(dialogService));
            _loader = modelLoader ?? throw new ArgumentNullException(nameof(modelLoader));

            _appData.PropertyChanged += OnAppDataPropertyChanged;
            _storageService.PropertyChanged += OnPropertyChanged;
        }

        private void OnAppDataPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(AppData.CurrentVault) && _appData.CurrentVault != null)
            {
                _appData.CurrentVault.PropertyChanged += OnPropertyChanged;
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(StorageService.NewStorageInfo))
            {
                Logger.D($"{GetType()}.OnPropertyChanged() - StorageService.NewStorageInfo");
                NewRecoveryInfo = null;
            }
            else if (e.PropertyName == nameof(Vault.IsChanged))
            {
                Logger.D($"{GetType()}.OnPropertyChanged() - Vault.IsChanged");
            }
        }

        public async Task AutoSave()
        {
            try
            {
                if (_appData.CurrentVault != null && _appData.CurrentVault.IsChanged && !IsSameAsLast)
                {
                    await SaveRecoveryFile();
                }
            }
            catch (IOException ex)
            {
                Logger.W($"{GetType()}.AutoSave() - Exception: {ex}");
            }
        }

        public async Task<bool> LoadRecoveryFile()
        {
            try
            {
                var settings = new LastFileSetting();
                var path = settings.Get("");

                if (!string.IsNullOrEmpty(path))
                {
                    IStorageFile recoveryFile = await _localFolderService.GetFileAsync(RECOVERY_FILE_NAME);

                    var fileName = Path.GetFileName(path);
                    Logger.D($"LoadRecoveryFile() - file exists.  Last file was '{fileName}'");

                    string password = await _dialogService.ShowEnterPasswordDialog("Load recovery file?", fileName);
                    if (!string.IsNullOrEmpty(password))
                    {
                        Vault v = await _loader.Load(recoveryFile, password);

                        v.IsChanged = true;
                        var cmd = new SetVault(v);
                        _commandService.Do(cmd);

                        _storageService.NewStorageInfo = new NewStorageInfo { Path = path, Password = password, IsReadOnly = false, Contents = new Vault(_appData.CurrentVault) };

                        DeleteRecoveryFile();
                        return true;
                    }
                    else
                    {
                        // User cancel, or recovery file and no last file info (not normally possible)
                        DeleteRecoveryFile();
                        return false;
                    }
                }
            }
            catch (IOException ex)
            {
                Logger.W($"LoadRecoveryFile() - exception {ex}");
            }

            return false;
        }

        public async Task<bool> SaveRecoveryFile()
        {
            Logger.D("SaveRecoveryFile() - create");

            IStorageFile recoveryFile = await _localFolderService.CreateFileAsync(RECOVERY_FILE_NAME, CreationCollisionOption.ReplaceExisting);
            if (recoveryFile == null) return false;

            if (_storageService.NewStorageInfo != null && !string.IsNullOrEmpty(_storageService.NewStorageInfo.Password))
            {
                var password = _storageService.NewStorageInfo.Password;
                Logger.D("SaveRecoveryFile() - saving");

                await _loader.Save(_appData.CurrentVault, recoveryFile, password);

                NewRecoveryInfo = new NewStorageInfo { Path = recoveryFile.Path, Password = password, IsReadOnly = false, Contents = new Vault(_appData.CurrentVault) };

                return true;
            }
            else
            {
                Logger.W("Auto-save not possible (no password)");
            }

            return false;
        }

        public void DeleteRecoveryFile()
        {
            Logger.D("DeleteRecoveryFile() - delete");

            _localFolderService.DeleteFile(RECOVERY_FILE_NAME);
            NewRecoveryInfo = null;
        }
    }
}
