﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Models.Settings;
using System.Threading.Tasks;

namespace LoginVault.Services
{
    public class ExampleVaultService
    {
        private readonly AppData _appData;
        private readonly CommandService _viewCommandService;

        public ExampleVaultService(AppData appData, CommandService viewCommandService)
        {
            _appData = appData;
            _viewCommandService = viewCommandService;
        }

        public async Task<bool> OfferExampleVault()
        {
            IStringSetting settings = new LocalStringSetting();
            string doneOnceOnly = settings.GetSetting(ONCE_ONLY_DONE_KEY, ONCE_ONLY_DONE_VALUE);
            if (doneOnceOnly != ONCE_ONLY_DONE_VALUE)
            {
                settings.SetSetting(ONCE_ONLY_DONE_KEY, ONCE_ONLY_DONE_VALUE);

                PopupService dlg = new PopupService();
                bool createExample = await dlg.ShowOfferExampleDialog();
                if (createExample)
                {
                    var seq = new Sequence(
                                                    new AddExampleVault(),
                                                    new SelectLocation(new CardLocation(0))
                                                    );

                    _viewCommandService.Do(seq);

                    return true;
                }
            }

            return false;
        }

        private const string ONCE_ONLY_DONE_KEY = "OnceOnlyDone";
        private const string ONCE_ONLY_DONE_VALUE = "done";
    }
}
