﻿using LoginVault.Models;
using System.ComponentModel;
using System.Threading.Tasks;

namespace LoginVault.Services
{
#nullable enable

    public interface IStorageService
    {
        bool IsSameAsLast { get; }

        void Clear();
        Task SaveOrDiscardChanges();

        Task<bool> LoadLastFile();
        Task<bool> LoadPickedFile();

        Task<bool> SaveCurrentFile();
        Task<bool> SavePickedFile();

        event PropertyChangedEventHandler PropertyChanged;
        NewStorageInfo? NewStorageInfo { get; }
    }
}
