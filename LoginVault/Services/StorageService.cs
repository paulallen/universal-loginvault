﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Models.Settings;
using LoginVault.Mvvm;
using LoginVault.Serialization;
using System;
using System.IO;
using System.Threading.Tasks;
using Utils;
using Windows.Storage;
using Windows.Storage.AccessCache;

namespace LoginVault.Services
{
#nullable enable

    public class StorageService : Observable, IStorageService
    {
        private readonly AppData _appData;
        private readonly CommandService _commandService;
        private readonly IDialogService _dialogService;
        private readonly IPopupService _popupService;
        private readonly IFilePicker _pickers;
        private readonly IModelLoader _loader;

        public IRecoveryFileService? RecoveryFileService { get; set; }

        private NewStorageInfo? _newStorageInfo;
        public NewStorageInfo? NewStorageInfo
        {
            get { return _newStorageInfo; }
            set { SetField(ref _newStorageInfo, value); }
        }

        public bool IsSameAsLast => _newStorageInfo != null && _appData.CurrentVault != null && _appData.CurrentVault.IsSameAs(_newStorageInfo.Contents);

        public StorageService(AppData appData, CommandService commandService, IDialogService dialogService, IPopupService popupService, IFilePicker openPicker, IModelLoader modelLoader)
        {
            _appData = appData ?? throw new ArgumentNullException(nameof(appData));
            _commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));
            _dialogService = dialogService ?? throw new ArgumentNullException(nameof(dialogService));
            _popupService = popupService ?? throw new ArgumentNullException(nameof(popupService));
            _pickers = openPicker ?? throw new ArgumentNullException(nameof(openPicker));
            _loader = modelLoader ?? throw new ArgumentNullException(nameof(modelLoader));
        }

        public void Clear()
        {
            // Clear current state
            NewStorageInfo = null;

            // Don't clear _lastFileService
        }

        public async Task SaveOrDiscardChanges()
        {
            if (!IsSameAsLast)
            {
                bool ok = await _popupService.ShowSaveChangesDialog();
                if (ok)
                {
                    await SaveCurrentFile();
                }
            }
        }

        public async Task<bool> LoadLastFile()
        {
            var settings = new LastFileSetting();
            var path = settings.Get("");

            if (string.IsNullOrEmpty(path)) return false;

            try
            {
                IStorageFile lastFile = await StorageFile.GetFileFromPathAsync(path);
                if (lastFile != null)
                {
                    string password = await _dialogService.ShowEnterPasswordDialog("Load previous file?", lastFile.Name);
                    if (!string.IsNullOrEmpty(password))
                    {
                        return await LoadVault(lastFile, password);
                    }
                }
            }
            catch (IOException ex)
            {
                Logger.W($"File '{path}' exception: {ex}");
            }

            return false;
        }

        public async Task<bool> LoadPickedFile()
        {
            IStorageFile pickedFile = await _pickers.PickOpenFile();
            if (pickedFile != null)
            {
                return await LoadFile(pickedFile);
            }

            return false;
        }

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped
        public async Task<bool> LoadFile(IStorageFile file)
#pragma warning restore S4457 // Parameter validation in "async"/"await" methods should be wrapped
        {
            if (file == null) throw new ArgumentNullException(nameof(file));

            string password = await _dialogService.ShowEnterPasswordDialog("Password for file", file.Name);
            if (!string.IsNullOrEmpty(password))
            {
                return await LoadVault(file, password);
            }

            return false;
        }

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped
        private async Task<bool> LoadVault(IStorageFile file, string password)
#pragma warning restore S4457 // Parameter validation in "async"/"await" methods should be wrapped
        {
            if (file == null) throw new ArgumentNullException(nameof(file));
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (RecoveryFileService == null) throw new InvalidOperationException("No RecoveryFileService");

            try
            {
                Logger.D($"LoadVault() - loading file: {file.Path}");

                Vault v = await _loader.Load(file, password);

                var cmd = new SetVault(v);
                _commandService.Do(cmd);

                UpdateMyState(file, password);

                RecoveryFileService.DeleteRecoveryFile();

                return true;
            }
            catch (IOException ex)
            {
                Logger.W($"LoadVault() - file: {file.Path}, exception: {ex}");
                await _popupService.ShowOpenFailMessage();
                return false;
            }
        }


        public async Task<bool> SaveCurrentFile()
        {
            try
            {
                if (NewStorageInfo != null && !string.IsNullOrEmpty(NewStorageInfo.Path) && !string.IsNullOrEmpty(NewStorageInfo.Password))
                {
                    IStorageFile currentFile = await StorageFile.GetFileFromPathAsync(NewStorageInfo.Path);
                    if (currentFile != null && !currentFile.Attributes.HasFlag(Windows.Storage.FileAttributes.ReadOnly))
                    {
                        return await SaveVault(currentFile, NewStorageInfo.Password);
                    }
                }
                else
                {
                    return await SavePickedFile();
                }
            }
            catch (IOException ex)
            {
                Logger.W($"File exception: {ex}");
            }

            return false;
        }

        public async Task<bool> SavePickedFile()
        {
            IStorageFile pickedFile = await _pickers.PickSaveFile();
            if (pickedFile != null)
            {
                string password = await _dialogService.ShowSetPasswordDialog(pickedFile.Name);
                if (!string.IsNullOrEmpty(password))
                {
                    return await SaveVault(pickedFile, password);
                }
            }

            return false;
        }

#pragma warning disable S4457 // Parameter validation in "async"/"await" methods should be wrapped
        private async Task<bool> SaveVault(IStorageFile file, string password)
#pragma warning restore S4457 // Parameter validation in "async"/"await" methods should be wrapped
        {
            if (file == null) throw new ArgumentNullException(nameof(file));
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (RecoveryFileService == null) throw new InvalidOperationException("No RecoveryFileService");

            try
            {
                if (_appData.CurrentVault == null) return false;

                Logger.D($"SaveVault() - saving vault to file: {file.Path}");

                await _loader.Save(_appData.CurrentVault, file, password);
                _appData.CurrentVault.NotChanged();

                UpdateMyState(file, password);

                RecoveryFileService.DeleteRecoveryFile();

                return true;
            }
            catch (IOException ex)
            {
                Logger.E($"IntSave() - file: {file.Path}, exception: {ex}");
                await _popupService.ShowSaveFailMessage();
                return false;
            }
        }

        private void UpdateMyState(IStorageFile file, string password)
        {
            if (_appData.CurrentVault == null) return;

            StorageApplicationPermissions.FutureAccessList.Add(file);

            NewStorageInfo = new NewStorageInfo { Path = file.Path, Password = password, IsReadOnly = file.Attributes.HasFlag(Windows.Storage.FileAttributes.ReadOnly), Contents = new Vault(_appData.CurrentVault) };

            var settings = new LastFileSetting();
            settings.Set(file.Path);
        }
    }
}
