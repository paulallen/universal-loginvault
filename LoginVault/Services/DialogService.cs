﻿using Services.Dialogs;
using System;
using System.Threading.Tasks;
using Utils;

namespace LoginVault.Services
{
    public sealed class DialogService : IDialogService
    {
        private static readonly DialogService _instance = new DialogService();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static DialogService()
        {
        }

        private DialogService()
        {
        }

        public static DialogService Instance
        {
            get
            {
                return _instance;
            }
        }

        public async Task<string> ShowEditNameDialog(string name)
        {
            Logger.D($"ShowEditNameDialog(name: '{name}')");

            if (_isInDialog) return "";

            EditNameDialog dialog = new EditNameDialog();
            EditNameDialogVm vm = new EditNameDialogVm(dialog, name);
            dialog.DataContext = vm;

            _isInDialog = true;
            await dialog.ShowAsync();
            _isInDialog = false;

            if (vm.Result == DialogVm.DialogResult.OK)
            {
                return vm.Name;
            }

            return "";
        }

        public async Task<string> ShowEnterPasswordDialog(string prompt, string fileName)
        {
            Logger.D($"ShowEnterPasswordDialog(prompt: '{prompt}', fileName: '{fileName}')");

            if (_isInDialog) return "";

            EnterPasswordDialog dialog = new EnterPasswordDialog();
            EnterPasswordDialogVm vm = new EnterPasswordDialogVm(dialog, prompt, fileName);
            dialog.DataContext = vm;

            _isInDialog = true;
            await dialog.ShowAsync();
            _isInDialog = false;

            if (vm.Result == DialogVm.DialogResult.OK)
            {
                return vm.Password;
            }

            return "";
        }

        public async Task<string> ShowSetPasswordDialog(string fileName)
        {
            Logger.D($"ShowSetPasswordDialog(fileName: '{fileName}')");

            if (_isInDialog) return "";

            SetPasswordDialog dialog = new SetPasswordDialog();
            SetPasswordDialogVm vm = new SetPasswordDialogVm(dialog, fileName);
            dialog.DataContext = vm;

            _isInDialog = true;
            await dialog.ShowAsync();
            _isInDialog = false;

            if (vm.Result == DialogVm.DialogResult.OK)
            {
                return vm.Password;
            }

            return "";
        }

        private bool _isInDialog;
    }
}
