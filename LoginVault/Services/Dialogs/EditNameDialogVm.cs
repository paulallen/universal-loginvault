﻿using Windows.UI.Xaml.Controls;

namespace Services.Dialogs
{
    public class EditNameDialogVm : DialogVm
    {
        public string Name { get; set; }

        public EditNameDialogVm(ContentDialog dialog, string name)
            : base(dialog)
        {
            Name = name;
        }
    }
}
