﻿using Windows.UI.Xaml.Controls;

namespace Services.Dialogs
{
    public class EnterPasswordDialogVm : DialogVm
    {
        public string Prompt { get; private set; }
        public string Filename { get; private set; }
        public string Password { get; set; }

        public EnterPasswordDialogVm(ContentDialog dialog, string prompt, string filename)
            : base(dialog)
        {
            Prompt = prompt;
            Filename = filename;
            Password = "";
        }
    }
}
