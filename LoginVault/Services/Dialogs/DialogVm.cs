﻿using LoginVault.Mvvm;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;

namespace Services.Dialogs
{
    public class DialogVm : Observable
    {
        public enum DialogResult { OK, Cancel }

        public ContentDialog Dialog { get; set; }
        public DialogResult Result { get; private set; }

        public ICommand CmdOk => new RelayCommand(Ok);
        public ICommand CmdCancel => new RelayCommand(Cancel);

        public DialogVm(ContentDialog dialog)
        {
            Result = DialogResult.Cancel;
            Dialog = dialog;
        }

        private void Ok()
        {
            Result = DialogResult.OK;
            Dialog.Hide();
        }

        private void Cancel()
        {
            Result = DialogResult.Cancel;
            Dialog.Hide();
        }
    }
}
