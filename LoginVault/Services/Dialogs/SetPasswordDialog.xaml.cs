﻿namespace Services.Dialogs
{
    public sealed partial class SetPasswordDialog
    {
        public SetPasswordDialog()
        {
            InitializeComponent();
        }

        private void Grid_KeyUp(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (!(DataContext is SetPasswordDialogVm vm)) return;

            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                vm.CmdOk.Execute(null);
            }
            else if (e.Key == Windows.System.VirtualKey.Escape)
            {
                vm.CmdCancel.Execute(null);
            }
        }
    }
}
