﻿using Windows.UI.Xaml.Controls;

namespace Services.Dialogs
{
    public class SetPasswordDialogVm : DialogVm
    {
        public string Filename { get; private set; }

        private string _password = "";
        public string Password
        {
            get { return _password; }
            set
            {
                SetField(ref _password, value);
                InvokePropertyChanged(nameof(IsValid));
            }
        }

        private string _repeat = "";
        public string RepeatPassword
        {
            get { return _repeat; }
            set
            {
                SetField(ref _repeat, value);
                InvokePropertyChanged(nameof(IsValid));
            }
        }

        public bool IsValid
        {
            get { return Password == RepeatPassword; }
        }

        public SetPasswordDialogVm(ContentDialog dialog, string filename)
            : base(dialog)
        {
            Filename = filename;
        }
    }
}
