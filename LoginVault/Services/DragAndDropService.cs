﻿using LoginVault.Commands;
using LoginVault.Models;
using System;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace LoginVault.Services
{
    public class DragAndDropService
    {
        private readonly AppData _appData;
        private readonly CommandService _viewCommandService;

        public DragAndDropService(AppData appData, CommandService viewCommandService)
        {
            _appData = appData;
            _viewCommandService = viewCommandService;
        }

        static public void CopyTextToClipboard(string text)
        {
            DataPackage dataPackage = new DataPackage();
            dataPackage.SetText(text);
            Clipboard.SetContent(dataPackage);
        }

        public void DragItemStarting(DragItemsStartingEventArgs e)
        {
            if (e == null) return;
            if (e.Items == null || e.Items.Count == 0) return;
            if (!(e.Items[0] is Card card)) return;

            var location = _appData.CurrentVault.FindCardLocationOfCard(card);
            string text = $"{CARD},{location.FolderIndex},{location.CardIndex}";
            e.Data.SetText(text);
        }

        public static void DragOver(DragEventArgs e)
        {
            if (e == null) return;

            e.AcceptedOperation = DataPackageOperation.Move;
        }

        public async void Drop(object sender, DragEventArgs e)
        {
            if (e == null) return;
            if (e.Data == null) return;
            if (_appData.CurrentVault == null) return;

            DataPackageView view = e.Data.GetView();
            if (view == null) return;

            if (!view.Contains(StandardDataFormats.Text)) return;

            string text = await view.GetTextAsync();
            if (text == null) return;
            if (!text.StartsWith(CARD, StringComparison.Ordinal)) return;

            string[] bits = text.Split(',');
            if (bits.Length != 3) return;

            string folderIndexString = bits[1];
            bool folderOk = int.TryParse(folderIndexString, out int folderNumberIndex);
            if (!folderOk) return;

            string cardIndexString = bits[2];
            bool cardOk = int.TryParse(cardIndexString, out int cardNumberIndex);
            if (!cardOk) return;

            Folder fromFolder = _appData.CurrentVault.Folders[folderNumberIndex];
            Card card = fromFolder.Cards[cardNumberIndex];

            if (!(sender is Border target)) return;
            if (!(target.DataContext is Folder folder)) return;

            var folderIndex = _appData.CurrentVault.Folders.IndexOf(folder);

            var seq = new Sequence(
                new MoveCardToFolder(_appData.CurrentVault.FindCardLocationOfCard(card), folderIndex),
                new SortCards(folderIndex));

            _viewCommandService.Do(seq);

            e.Handled = true;
        }

        private const string CARD = "#card";
    }
}
