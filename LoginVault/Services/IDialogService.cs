﻿using System.Threading.Tasks;

namespace LoginVault.Services
{
    public interface IDialogService
    {
        Task<string> ShowEditNameDialog(string name);
        Task<string> ShowEnterPasswordDialog(string prompt, string fileName);
        Task<string> ShowSetPasswordDialog(string fileName);
    }
}
