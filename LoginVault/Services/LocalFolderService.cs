﻿using System;
using System.IO;
using System.Threading.Tasks;
using Utils;
using Windows.Storage;

namespace LoginVault.Services
{
    public class LocalFolderService : ILocalFolderService
    {
        public async Task<IStorageFile> CreateFileAsync(string fileName, CreationCollisionOption options)
        {
            return await ApplicationData.Current.LocalFolder.CreateFileAsync(fileName, options);
        }

        public async Task<IStorageFile> GetFileAsync(string fileName)
        {
            return await ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
        }

        public void DeleteFile(string fileName)
        {
            var path = Path.Combine(ApplicationData.Current.LocalFolder.Path, fileName);

            if (File.Exists(path))
            {
                File.Delete(path);
                Logger.D($"Deleted file: {path}");
            }
        }
    }
}
