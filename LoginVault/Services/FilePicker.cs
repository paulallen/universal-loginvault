﻿#pragma warning disable S1128 // Unused "using" should be removed
using System;
#pragma warning restore S1128 // Unused "using" should be removed
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;

namespace LoginVault.Services
{
    public class FilePicker : IFilePicker
    {
        public const string FileExtension = ".lv1";

        public async Task<IStorageFile> PickOpenFile()
        {
            FileOpenPicker p = new FileOpenPicker
            {
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary
            };

            p.FileTypeFilter.Add(FileExtension);

            return await p.PickSingleFileAsync();
        }

        public async Task<IStorageFile> PickSaveFile()
        {
            FileSavePicker p = new FileSavePicker
            {
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary
            };

            p.FileTypeChoices.Add("Login Vault", new List<string>() { FileExtension });

            return await p.PickSaveFileAsync();
        }
    }
}
