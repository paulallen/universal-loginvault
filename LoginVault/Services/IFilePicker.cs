﻿using System.Threading.Tasks;
using Windows.Storage;

namespace LoginVault.Services
{
    public interface IFilePicker
    {
        Task<IStorageFile> PickOpenFile();
        Task<IStorageFile> PickSaveFile();
    }
}
