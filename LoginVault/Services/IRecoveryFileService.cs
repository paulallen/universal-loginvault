﻿using LoginVault.Models;
using System.Threading.Tasks;

namespace LoginVault.Services
{
#nullable enable

    public interface IRecoveryFileService
    {
        Task AutoSave();
        Task<bool> LoadRecoveryFile();
        Task<bool> SaveRecoveryFile();
        void DeleteRecoveryFile();

        NewStorageInfo? NewRecoveryInfo { get; }
        bool IsSameAsLast { get; }
    }
}
