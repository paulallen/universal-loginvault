﻿namespace LoginVault.Services
{
    public class PasswordHelper : IPasswordHelper
    {
        public string Rate(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return "None!";
            }
            else if (IsInCommonList(password))
            {
                return "Very Common!";
            }
            else if (password.Length < 8)
            {
                return "Weak";
            }
            else if (password.Length < 16)
            {
                return "Okay";
            }
            else
            {
                return "Strong";
            }
        }

        public string CheckPasswords(string password, string repeatPassword)
        {
            string message = "";

            if (repeatPassword != password)
            {
                message = "Does not match";
            }

            return message;
        }

        private bool IsInCommonList(string password)
        {
            bool bIn = false;

            foreach (string s in _commonPasswords)
            {
                if (password == s)
                {
                    bIn = true;
                    break;
                }
            }

            return bIn;
        }

        private static readonly string[] _commonPasswords =
        {
                "password",
                "qwerty",
                "football",
                "princess",
                "solo",
                "pussy",
                "dragon",
                "1234",
                "12345",
                "123456",
                "1234567",
                "12345678",
                "abc123",
                "batman",
                "letmein",
                "fuckyou",
         };
    }
}
