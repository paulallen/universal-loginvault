﻿using System.Threading.Tasks;
using Windows.Storage;

namespace LoginVault.Services
{
    public interface ILocalFolderService
    {
        Task<IStorageFile> CreateFileAsync(string fileName, CreationCollisionOption options);
        Task<IStorageFile> GetFileAsync(string fileName);
        void DeleteFile(string fileName);
    }
}
