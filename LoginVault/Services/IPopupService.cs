﻿using System;
using System.Threading.Tasks;

namespace LoginVault.Services
{
    public interface IPopupService
    {
        Task<bool> ShowOfferExampleDialog();
        Task<bool> ShowSaveChangesDialog();
        Task ShowOpenFailMessage();
        Task ShowSaveFailMessage();
        Task ShowUnhandledException(Exception ex);
    }
}
