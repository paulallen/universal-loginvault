﻿using LoginVault.Commands;
using LoginVault.Models;
using System;
using System.Collections.Generic;
using Utils;

namespace LoginVault.Services
{
    public class CommandService
    {
        private readonly AppData _appData;
        private readonly IList<BaseCommand> _commands;
        private int _nextIndex;

        public CommandService(AppData appData)
        {
            _appData = appData ?? throw new ArgumentNullException(nameof(appData));
            _commands = new List<BaseCommand>();
            _nextIndex = 0;
        }

        public void Clear()
        {
            _commands.Clear();
            _nextIndex = 0;
        }

        public void Do(BaseCommand cmd)
        {
            if (cmd == null) throw new ArgumentNullException(nameof(cmd));

            cmd.Execute(_appData);

            while (_nextIndex < _commands.Count)
            {
                _commands.RemoveAt(_commands.Count - 1);
            }

            _commands.Add(cmd);
            _nextIndex = _commands.Count;
        }

        public bool Undo()
        {
            if (_nextIndex == 0)
            {
                Logger.D("Undo() - end.");
                return false;
            }

            --_nextIndex;

            BaseCommand cmd = _commands[_nextIndex];
            cmd.Revert(_appData);

            return true;
        }

        public bool Redo()
        {
            if (_nextIndex == _commands.Count)
            {
                Logger.D("Redo() - end.");
                return false;
            }

            BaseCommand cmd = _commands[_nextIndex];
            cmd.Execute(_appData);

            ++_nextIndex;

            return true;
        }
    }
}
