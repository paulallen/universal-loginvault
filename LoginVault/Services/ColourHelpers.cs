﻿using System;
using System.Globalization;
using Windows.UI;

namespace LoginVault.Services
{
    static public class ColourHelpers
    {
        static public Color FromArgb(string argb)
        {
            if (argb != null && argb[0] == '#' && argb.Length == 9)
            {
                string sA = argb.Substring(1, 2);
                string sR = argb.Substring(3, 2);
                string sG = argb.Substring(5, 2);
                string sB = argb.Substring(7, 2);

                byte a = byte.Parse(sA, NumberStyles.HexNumber, CultureInfo.InvariantCulture.NumberFormat);
                byte r = byte.Parse(sR, NumberStyles.HexNumber, CultureInfo.InvariantCulture.NumberFormat);
                byte g = byte.Parse(sG, NumberStyles.HexNumber, CultureInfo.InvariantCulture.NumberFormat);
                byte b = byte.Parse(sB, NumberStyles.HexNumber, CultureInfo.InvariantCulture.NumberFormat);

                return Color.FromArgb(a, r, g, b);
            }

            return new Color();
        }

        static public string ToArgb(Color colour)
        {
            byte a = Convert.ToByte(colour.A);
            byte r = Convert.ToByte(colour.R);
            byte g = Convert.ToByte(colour.G);
            byte b = Convert.ToByte(colour.B);

            return string.Format(CultureInfo.InvariantCulture.NumberFormat, "#{0:X2}{1:X2}{2:X2}{3:X2}", a, r, g, b);
        }
    }
}
