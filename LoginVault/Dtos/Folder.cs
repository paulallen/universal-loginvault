﻿using System.Collections.Generic;

namespace LoginVault.Dtos
{
    public class Folder
    {
        public string Name { get; set; } = "";
        public List<Card> Cards { get; } = new List<Card>();
    }
}
