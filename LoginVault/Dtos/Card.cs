﻿namespace LoginVault.Dtos
{
    public class Card
    {
        public string Url { get; set; } = "";
        public string Username { get; set; } = "";
        public string Password { get; set; } = "";
        public string Other { get; set; } = "";
    }
}
