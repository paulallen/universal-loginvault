﻿using System.Collections.Generic;

namespace LoginVault.Dtos
{
    public class Box
    {
        public List<Folder> Folders { get; } = new List<Folder>();
    }
}
