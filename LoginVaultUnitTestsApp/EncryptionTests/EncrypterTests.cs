﻿using LoginVault.Encryption;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using Windows.Storage.Streams;

namespace EncryptionTests
{
    [TestClass]
    public class EncrypterTests
    {
        private const byte PAYLOAD_SALT_A_LEN = 13;
        private const byte PAYLOAD_SALT_B_LEN = 7;
        private const byte BLOCKSIZE_BYTES = 16;
        private const byte KEYSIZE_BYTES = 32;
        private const byte MESSAGESIZE_BYTES = 128;

        [TestMethod]
        public void ReadBufferRefStartCorrect()
        {
            IBuffer ib1 = TestBuffers.NewTestBuffer(MESSAGESIZE_BYTES, 0xbc);

            uint[] auLengths = { 1, 2, 4, 8, 16, 32 };
            foreach (uint uLength in auLengths)
            {
                IBuffer ibTest;
                uint uStart = 0;
                for (int n = 0; n < MESSAGESIZE_BYTES / uLength; ++n)
                {
                    Encrypter.ReadBuffer(ib1, out ibTest, ref uStart, uLength);

                    Assert.IsTrue(ibTest.Length == uLength);

                    uint uTest = (uint)(n * uLength) + uLength;
                    Assert.IsTrue(uStart == uTest);
                }
            }
        }

        [TestMethod]
        public void ReadBufferOutputBytesCorrect()
        {
            IBuffer ib1 = TestBuffers.NewTestBuffer(1, 0x10);
            IBuffer ib2 = TestBuffers.NewTestBuffer(2, 0x20);
            IBuffer ib3 = TestBuffers.NewTestBuffer(3, 0x30);
            IBuffer ib4 = TestBuffers.NewTestBuffer(4, 0x40);

            IBuffer ibAll;
            using (DataWriter dw = new DataWriter())
            {
                dw.WriteBuffer(ib1);
                dw.WriteBuffer(ib2);
                dw.WriteBuffer(ib3);
                dw.WriteBuffer(ib4);

                ibAll = dw.DetachBuffer();
            }

            uint uStart = 0;
            IBuffer ibTest1, ibTest2, ibTest3, ibTest4;

            Encrypter.ReadBuffer(ibAll, out ibTest1, ref uStart, 1);
            Encrypter.ReadBuffer(ibAll, out ibTest2, ref uStart, 2);
            Encrypter.ReadBuffer(ibAll, out ibTest3, ref uStart, 3);
            Encrypter.ReadBuffer(ibAll, out ibTest4, ref uStart, 4);

            Assert.IsTrue(TestBuffers.AreEqual(ibTest1, ib1));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest2, ib2));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest3, ib3));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest4, ib4));
        }

        [TestMethod]
        public void SaltedPasswordInExpectedOrder()
        {
            IBuffer ibFirst = TestBuffers.NewTestBuffer(KEYSIZE_BYTES, 0xbc);
            IBuffer ibSecond = TestBuffers.NewTestBuffer(KEYSIZE_BYTES, 0xde);

            IBuffer ibSalted = Encrypter.GenerateSaltedPassword(ibFirst, ibSecond);

            uint uStart = 0;
            IBuffer ibTest1, ibTest2;
            Encrypter.ReadBuffer(ibSalted, out ibTest1, ref uStart, KEYSIZE_BYTES);
            Encrypter.ReadBuffer(ibSalted, out ibTest2, ref uStart, KEYSIZE_BYTES);

            Assert.IsTrue(ibSalted.Length == ibFirst.Length + ibSecond.Length);
            Assert.IsTrue(TestBuffers.AreEqual(ibTest1, ibFirst));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest2, ibSecond));
        }

        [TestMethod]
        public void PayloadAssemblyRoundTripWorks()
        {
            IBuffer ib1 = TestBuffers.NewTestBuffer(PAYLOAD_SALT_A_LEN, 0xbc);
            IBuffer ib2 = TestBuffers.NewTestBuffer(KEYSIZE_BYTES, 0xcd);
            IBuffer ib3 = TestBuffers.NewTestBuffer(BLOCKSIZE_BYTES, 0xa0);
            IBuffer ib4 = TestBuffers.NewTestBuffer(MESSAGESIZE_BYTES, 0xc2);
            IBuffer ib5 = TestBuffers.NewTestBuffer(PAYLOAD_SALT_B_LEN, 0xd3);

            IBuffer ibAll = Encrypter.AssemblePayload(ib1, ib2, ib3, ib4, ib5);

            Assert.IsTrue(ibAll.Length == PAYLOAD_SALT_A_LEN + KEYSIZE_BYTES + BLOCKSIZE_BYTES + MESSAGESIZE_BYTES + PAYLOAD_SALT_B_LEN);


            IBuffer ibTest1, ibTest2, ibTest3, ibTest4, ibTest5;

            Encrypter.DisassemblePayload(ibAll, out ibTest1, out ibTest2, out ibTest3, out ibTest4, out ibTest5);

            Assert.IsTrue(TestBuffers.AreEqual(ibTest1, ib1));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest2, ib2));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest3, ib3));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest4, ib4));
            Assert.IsTrue(TestBuffers.AreEqual(ibTest5, ib5));
        }

        [TestMethod]
        public void BufferRoundTripWorks()
        {
            const byte PADDED_SIZE = 5 * BLOCKSIZE_BYTES;

            string password = "alongpassword";
            IBuffer ibPadded = TestBuffers.NewTestBuffer(PADDED_SIZE, 0x06);

            IBuffer ibCypher = Encrypter.EncryptBuffer(ibPadded, password);
            IBuffer ibDecrypted = Encrypter.DecryptBuffer(ibCypher, password);

            Assert.IsTrue(TestBuffers.AreEqual(ibDecrypted, ibPadded));
        }

        [TestMethod]
        public void StringRoundTripWorks()
        {
            string message = "some text";
            string password = "apassword";

            Encrypter encrypter = new Encrypter();
            IBuffer ibCipherText = encrypter.Encrypt(message, password);

            string backWorks = encrypter.Decrypt(ibCipherText, password);
            Assert.IsTrue(backWorks == message);
        }

        [TestMethod]
        public void WrongPasswordThrows()
        {
            string message = "some text";
            string longpassword = "alongpassword";
            string password = longpassword.Substring(0, 8);

            Debug.WriteLine("Encrypting with password: '{0}'", password);
            Encrypter encrypter = new Encrypter();
            IBuffer ibCipherText = encrypter.Encrypt(message, password);

            for (int n = longpassword.Length - 1; n >= 0; --n)
            {
                string str = longpassword.Substring(0, n);
                if (str != password)
                {
                    Debug.WriteLine("Checking decrypting with password: '{0}'", str);

                    try
                    {
                        string decrypt = encrypter.Decrypt(ibCipherText, str);
                        Assert.IsTrue(false);
                    }
                    catch (Exception)
                    {
                        Assert.IsTrue(true);
                    }
                }
            }
        }
    }
}
