﻿using LoginVault.Encryption;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Windows.Storage.Streams;

namespace EncryptionTests
{
    [TestClass]
    public class PadderTests
    {
        [TestMethod]
        public void PadderSizeCalculationCorrect()
        {
            const byte BLOCKSIZE = 16;
            uint uLength;

            // < BLOCKSIZE
            for (uint u = 0; u < BLOCKSIZE; ++u)
            {
                uLength = Padder.PaddedBufferLength(u, BLOCKSIZE);
                Assert.IsTrue(uLength == BLOCKSIZE);
            }

            // BLOCKSIZE
            uLength = Padder.PaddedBufferLength(BLOCKSIZE, BLOCKSIZE);
            Assert.IsTrue(uLength == 2 * BLOCKSIZE);
        }

        [TestMethod]
        public void PadderAddWorks()
        {
            Padder paddy = new Padder();
            const byte BLOCKSIZE = 16;

            // < BLOCKSIZE
            for (byte nBytes = 1; nBytes < BLOCKSIZE; ++nBytes)
            {
                IBuffer ibMsg = TestBuffers.NewTestBuffer(nBytes, 0xc0);

                IBuffer ibPadded = paddy.Add(ibMsg, BLOCKSIZE);

                Assert.IsTrue(ibPadded.Length == BLOCKSIZE);

                DataReader dr = DataReader.FromBuffer(ibPadded);
                for (byte b = 0; b < nBytes; ++b)
                {
                    byte b1 = dr.ReadByte();
                    Assert.IsTrue(b1 == 0xc0 + b);
                }

                byte bTest = (byte)(BLOCKSIZE - nBytes);
                for (uint u = nBytes; u < BLOCKSIZE - 1; ++u)
                {
                    byte bPad = dr.ReadByte();
                    Assert.IsTrue(bPad == bTest);
                }
            }

            // BLOCKSIZE
            {
                IBuffer ibMsg = TestBuffers.NewTestBuffer(BLOCKSIZE, 0xd0);

                IBuffer ibPadded = paddy.Add(ibMsg, BLOCKSIZE);

                Assert.IsTrue(ibPadded.Length == 2 * BLOCKSIZE);

                DataReader dr = DataReader.FromBuffer(ibPadded);
                for (byte b = 0; b < BLOCKSIZE; ++b)
                {
                    byte b1 = dr.ReadByte();
                    Assert.IsTrue(b1 == 0xd0 + b);
                }

                for (uint u = BLOCKSIZE; u < 2 * BLOCKSIZE; ++u)
                {
                    byte bPad = dr.ReadByte();
                    Assert.IsTrue(bPad == BLOCKSIZE);
                }
            }
        }

        [TestMethod]
        public void PadderRoundTripWorks()
        {
            Padder paddy = new Padder();

            const byte BLOCKSIZE = 16;

            // < BLOCKSIZE
            for (byte nBytes = 1; nBytes < BLOCKSIZE; ++nBytes)
            {
                IBuffer ibMsg = TestBuffers.NewTestBuffer(nBytes, 0xa0);

                IBuffer ibPadded = paddy.Add(ibMsg, BLOCKSIZE);
                IBuffer ibStripped = paddy.Remove(ibPadded, BLOCKSIZE);

                Assert.IsTrue(ibStripped.Length == nBytes);
                Assert.IsTrue(TestBuffers.AreEqual(ibStripped, ibMsg));
            }

            // BLOCKSIZE
            {
                IBuffer ibMsg = TestBuffers.NewTestBuffer(BLOCKSIZE, 0xb0);

                IBuffer ibPadded = paddy.Add(ibMsg, BLOCKSIZE);
                IBuffer ibStripped = paddy.Remove(ibPadded, BLOCKSIZE);

                Assert.IsTrue(ibStripped.Length == BLOCKSIZE);
                Assert.IsTrue(TestBuffers.AreEqual(ibStripped, ibMsg));
            }
        }
    }
}
