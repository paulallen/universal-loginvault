﻿using Windows.Storage.Streams;

namespace EncryptionTests
{
    public static class TestBuffers
    {
        public static IBuffer NewTestBuffer(byte length, byte bStart)
        {
            DataWriter dw = new DataWriter();
            for (byte b = 0; b < length; ++b)
            {
                dw.WriteByte((byte)(bStart + b));
            }
            IBuffer ib = dw.DetachBuffer();
            return ib;
        }

        public static bool AreEqual(IBuffer ib1, IBuffer ib2)
        {
            if (ib1.Length != ib2.Length)
            {
                return false;
            }

            DataReader dr1 = DataReader.FromBuffer(ib1);
            DataReader dr2 = DataReader.FromBuffer(ib2);
            for (uint u = 0; u < ib1.Length; ++u)
            {
                byte b1 = dr1.ReadByte();
                byte b2 = dr2.ReadByte();
                if (b1 != b2)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
