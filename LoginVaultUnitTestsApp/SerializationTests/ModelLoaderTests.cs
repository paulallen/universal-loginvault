﻿using LoginVault.Models;
using LoginVault.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SerializationTests
{
    [TestClass]
    public class ModelLoaderTests
    {
        [TestMethod]
        public void TestMappingDtoRoundtrip()
        {
            var mapper = new DtoMapper();
            var vault = GetTestVault();

            var dto = mapper.ToDto(vault);
            var nv = mapper.FromDto(dto);

            Assert.IsTrue(nv.IsSameAs(vault));
        }

        [TestMethod]
        public void TestMappingDtoV2Roundtrip()
        {
            var mapper = new DtoV2Mapper();
            var vault = GetTestVault();

            var dto = mapper.ToDto(vault);
            var nv = mapper.FromDto(dto);

            Assert.IsTrue(nv.IsSameAs(vault));
        }

        [TestMethod]
        public void TestSerializationRountrip()
        {
            var serializer = new MySerializer();
            var vault = GetTestVault();

            var str = serializer.ToString(vault);
            var nv = serializer.FromString(str);

            Assert.IsTrue(nv.IsSameAs(vault));
        }

        [TestMethod]
        public void TestEncodingRoundtrip()
        {
            var testpassword = "testpassword";
            var serializer = new MySerializer();
            var encoder = new MyEncoder();
            var vault = GetTestVault();
            var plaintext = serializer.ToString(vault);

            var cipher = encoder.ToEncoded(plaintext, testpassword);
            var decoded = encoder.FromEncoded(cipher, testpassword);

            Assert.IsTrue(decoded == plaintext);
        }

        private Vault GetTestVault()
        {
            var vault = new Vault();

            Folder internet = new Folder() { Name = "Internet" };
            internet.Cards.Add(new Card() { Url = "news.myfavourite.com", Username = "bert", Password = "iPod Catastrophe", Other = "" });
            internet.Cards.Add(new Card() { Url = "film.reviews.com", Username = "bert", Password = "Yellow B33tle", Other = "" });
            internet.Cards.Add(new Card() { Url = "social.gossip.com", Username = "bert", Password = "RampantSnail!", Other = "" });

            Folder utilities = new Folder() { Name = "Utilities" };
            utilities.Cards.Add(new Card() { Url = "email.com", Username = "bert", Password = "Carp3t Ph0ne1", Other = "Last reading: 123456" });
            utilities.Cards.Add(new Card() { Url = "my.electricity.com", Username = "bert", Password = "WendyHouse4", Other = "" });
            utilities.Cards.Add(new Card() { Url = "my.gas.co.uk", Username = "ethel", Password = "Proper Cabbage #", Other = "Phone password: gghq1i7" });
            utilities.Cards.Add(new Card() { Url = "my.phonecompany.com", Username = "bertandethel", Password = "waste tractor 5", Other = "" });
            utilities.Cards.Add(new Card() { Url = "my.water.co.uk", Username = "bert", Password = "Tower Blue", Other = "" });

            Folder banking = new Folder() { Name = "Banking" };
            banking.Cards.Add(new Card() { Url = "account.bank.com", Username = "bert", Password = "Pudding Fault27", Other = "Frist pet: Cat" });
            banking.Cards.Add(new Card() { Url = "buildings.society.co.uk", Username = "ethel", Password = "Fungus shirt 8", Other = "Date: 22/3/1970" });
            banking.Cards.Add(new Card() { Url = "bonds.gov.uk", Username = "bert", Password = "Giant Toothbrush", Other = "" });

            vault.Folders.Add(internet);
            vault.Folders.Add(utilities);
            vault.Folders.Add(banking);

            return vault;
        }
    }
}
