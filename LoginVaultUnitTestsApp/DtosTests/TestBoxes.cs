﻿using LoginVault.Dtos;

namespace DtosTests
{
    public class TestBoxes
    {
        public static Box NewTestBox()
        {
            Card c = new Card
            {
                Url = "test.org",
                Username = "Bert",
                Password = "Ethel",
                Other = "Snake eyes"
            };

            Folder f = new Folder();
            f.Cards.Add(c);

            Box box = new Box();
            box.Folders.Add(f);

            return box;
        }

        public static bool AreEqual(Box box1, Box box2)
        {
            if (box1.Folders.Count != box2.Folders.Count)
            {
                return false;
            }

            for (int f = 0; f < box1.Folders.Count; ++f)
            {
                if (box1.Folders[f].Name != box2.Folders[f].Name)
                {
                    return false;
                }

                if (box1.Folders[f].Cards.Count != box2.Folders[f].Cards.Count)
                {
                    return false;
                }

                for (int c = 0; c < box1.Folders[f].Cards.Count; ++c)
                {
                    Card c1 = box1.Folders[f].Cards[c];
                    Card c2 = box2.Folders[f].Cards[c];

                    if (c1.Url != c2.Url ||
                        c1.Username != c2.Username ||
                        c1.Password != c2.Password ||
                        c1.Other != c2.Other)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
