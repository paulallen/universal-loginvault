﻿using LoginVault.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DtosTests
{
    [TestClass]
    public class BoxSerializerTests
    {
        [TestMethod]
        public void SerializerRoundTripWorks()
        {
            var box = TestBoxes.NewTestBox();
            var serializer = new BoxSerializer();

            string serialized = serializer.Serialize(box);
            var test = serializer.Deserialize(serialized);

            Assert.IsTrue(TestBoxes.AreEqual(box, test));
        }
    }
}
