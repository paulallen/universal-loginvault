﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServicesTests
{
    [TestClass]
    public class SortTests
    {
        private const string anne = "Anne";
        private const string bert = "Bert";
        private const string aaaa = "http://aaaa";
        private const string bbbb = "http://bbbb";

        [TestMethod]
        public void SortFoldersWorks()
        {
            var fA = new Folder { Name = anne };
            var fB = new Folder { Name = bert };

            var appData = new AppData();
            appData.CurrentVault.Folders.Add(fB);
            appData.CurrentVault.Folders.Add(fA);
            appData.SelectedLocation = new CardLocation(0, -1);

            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Name == bert);
            Assert.IsTrue(appData.CurrentVault.Folders[1].Name == anne);
            Assert.IsTrue(appData.SelectedLocation.FolderIndex == 0);

            var cmd = new SortFolders();
            var cs = new CommandService(appData);

            cs.Do(cmd);

            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Name == anne);
            Assert.IsTrue(appData.CurrentVault.Folders[1].Name == bert);
            Assert.IsTrue(appData.SelectedLocation.FolderIndex == 1);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Name == bert);
            Assert.IsTrue(appData.CurrentVault.Folders[1].Name == anne);
            Assert.IsTrue(appData.SelectedLocation.FolderIndex == 0);

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void SortCardsWorks()
        {
            var f = new Folder();
            var cA = new Card { Url = aaaa };
            var cB = new Card { Url = bbbb };
            f.Cards.Add(cB);
            f.Cards.Add(cA);

            var appData = new AppData();
            appData.CurrentVault.Folders.Add(f);
            appData.SelectedLocation = new CardLocation(0, 1);

            Assert.IsTrue(appData.CurrentVault.Folders.Count == 1);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards.Count == 2);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Url == bbbb);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[1].Url == aaaa);
            Assert.IsTrue(appData.SelectedLocation.CardIndex == 1);

            var cmd = new SortCards(0);
            var cs = new CommandService(appData);

            cs.Do(cmd);

            Assert.IsTrue(appData.CurrentVault.Folders.Count == 1);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards.Count == 2);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Url == aaaa);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[1].Url == bbbb);
            Assert.IsTrue(appData.SelectedLocation.CardIndex == 0);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 1);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards.Count == 2);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Url == bbbb);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[1].Url == aaaa);
            Assert.IsTrue(appData.SelectedLocation.CardIndex == 1);

            Assert.IsFalse(cs.Undo());
        }
    }
}
