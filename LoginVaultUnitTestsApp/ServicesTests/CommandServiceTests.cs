﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServicesTests
{
    [TestClass]
    public class CommandServiceTests
    {
        [TestMethod]
        public void CurrentVaultIsInitialized()
        {
            var appData = new AppData();

            Assert.IsNotNull(appData.CurrentVault);
            Assert.IsTrue(appData.CurrentVault.Name == "");
        }

        [TestMethod]
        public void SetVaultCycles()
        {
            var appData = new AppData();
            var cs = new CommandService(appData);

            var cmd = new SetVault(new Vault { Name = "Bert" });
            cs.Do(cmd);
            Assert.IsNotNull(appData.CurrentVault);
            Assert.IsTrue(appData.CurrentVault.Name == "Bert");

            cs.Undo();
            Assert.IsNotNull(appData.CurrentVault);
            Assert.IsTrue(appData.CurrentVault.Name == "");

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void CreateDeletedItemsCycles()
        {
            var appData = new AppData();
            var cs = new CommandService(appData);

            var deleted = new CreateDeletedItems();
            cs.Do(deleted);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 1);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 0);

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void AddFolderCycles()
        {
            var appData = new AppData();
            var cs = new CommandService(appData);

            var cmd = new AddFolder(new Folder());
            cs.Do(cmd);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 1);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 0);

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void RenameFolderCycles()
        {
            var f = new Folder { Name = "test" };
            var v = new Vault();
            v.Folders.Add(f);
            var appData = new AppData();
            appData.CurrentVault = v;
            var cs = new CommandService(appData);

            var newName = "fred";
            var cmd = new RenameFolder(0, newName);
            cs.Do(cmd);
            Assert.IsTrue(appData.CurrentVault.Folders[0] == f);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Name == newName);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders[0] == f);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Name == f.Name);

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void DeleteFolderCycles()
        {
            var f = new Folder { Name = "test" };
            var v = new Vault();
            v.Folders.Add(f);
            var appData = new AppData();
            appData.CurrentVault = v;
            var cs = new CommandService(appData);

            var deleted = new CreateDeletedItems();
            cs.Do(deleted);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);

            var cmd = new DeleteFolder(appData.CurrentVault.Folders.IndexOf(f));
            cs.Do(cmd);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 1);
            Assert.IsTrue(appData.CurrentVault.Folders.IndexOf(f) == -1);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);
            Assert.IsTrue(appData.CurrentVault.Folders.IndexOf(f) >= 0);

            cs.Undo(); // Deleted items add
            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void AddCardNoFolderDoesNothing()
        {
            var appData = new AppData();
            var cs = new CommandService(appData);

            var cmd = new AddCard(0, new Card());
            cs.Do(cmd);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 0);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 0);

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void AddCardCycles()
        {
            var appData = new AppData();
            appData.CurrentVault = new Vault();
            appData.CurrentVault.Folders.Add(new Folder());
            var cs = new CommandService(appData);

            var cmd = new AddCard(0, new Card());
            cs.Do(cmd);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards.Count == 1);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards.Count == 0);

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void UpdateCardCycles()
        {
            var c = new Card { Url = "http://blah", Username = "dave", Password = "hens", Other = "frisbees" };
            var f = new Folder { Name = "test" };
            f.Cards.Add(c);
            var v = new Vault();
            v.Folders.Add(f);
            var appData = new AppData();
            appData.CurrentVault = v;
            var cs = new CommandService(appData);

            var newUrl = "http://duckduckgo";
            var newUsername = "bert";
            var newPassword = "dogs";
            var newOther = "signs";
            var newCard = new Card { Url = newUrl, Username = newUsername, Password = newPassword, Other = newOther };

            var cmd = new UpdateCard(0, 0, newCard);
            cs.Do(cmd);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0] == c);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Url == newUrl);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Username == newUsername);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Password == newPassword);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Other == newOther);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0] == c);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Url == c.Url);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Username == c.Username);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Password == c.Password);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards[0].Other == c.Other);

            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void DeleteCardCycles()
        {
            var c = new Card { Url = "http://blah" };
            var f = new Folder { Name = "test" };
            f.Cards.Add(c);
            var v = new Vault();
            v.Folders.Add(f);
            var appData = new AppData();
            appData.CurrentVault = v;
            var cs = new CommandService(appData);

            var deleted = new CreateDeletedItems();
            cs.Do(deleted);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);

            var backups = appData.CurrentVault.GetBackupsFolder();
            var backupsIndex = appData.CurrentVault.Folders.IndexOf(backups);

            var location = appData.CurrentVault.FindCardLocationOfCard(c);
            var cmd = new DeleteCard(location.FolderIndex, location.CardIndex);
            cs.Do(cmd);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);
            Assert.IsTrue(appData.CurrentVault.FindCardLocationOfCard(c).FolderIndex == backupsIndex);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 2);
            Assert.IsTrue(appData.CurrentVault.FindCardLocationOfCard(c).FolderIndex != backupsIndex);

            cs.Undo(); // Deleted items add
            Assert.IsFalse(cs.Undo());
        }

        [TestMethod]
        public void MoveCardCycles()
        {
            var c = new Card { Url = "http://blah" };
            var f1 = new Folder { Name = "source" };
            var f2 = new Folder { Name = "destination" };
            f1.Cards.Add(c);
            var v = new Vault();
            v.Folders.Add(f1);
            v.Folders.Add(f2);
            var appData = new AppData();
            appData.CurrentVault = v;
            var cs = new CommandService(appData);

            var sourceLocation = v.FindCardLocationOfCard(c);
            Assert.IsTrue(sourceLocation.FolderIndex == 0);

            var cmd = new MoveCardToFolder(sourceLocation, 1);
            cs.Do(cmd);
            var destination = v.FindCardLocationOfCard(c);
            Assert.IsTrue(destination.FolderIndex == 1);

            cs.Undo();
            var revertLocation = v.FindCardLocationOfCard(c);
            Assert.IsTrue(revertLocation.FolderIndex == 0);

            cs.Undo(); // Deleted items add
            Assert.IsFalse(cs.Undo());
        }
    }
}
