﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServicesTests
{
    [TestClass]
    public class SequenceTests
    {
        [TestMethod]
        public void BasicSequenceCycles()
        {
            var appData = new AppData();
            Card c = new Card();
            var f = new Folder();

            var cmdAddFolder = new AddFolder(f);
            var cmdAddCard = new AddCard(0, c);
            var seq = new Sequence(cmdAddFolder, cmdAddCard);

            var cs = new CommandService(appData);

            cs.Do(seq);
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 1);
            Assert.IsTrue(appData.CurrentVault.Folders[0].Cards.Count == 1);

            cs.Undo();
            Assert.IsTrue(appData.CurrentVault.Folders.Count == 0);

            Assert.IsFalse(cs.Undo());
        }
    }
}
