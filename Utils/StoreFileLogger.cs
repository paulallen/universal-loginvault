﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;

namespace Utils
{
    public class StoreFileLogger : ILogger, ISuspendable, IDisposable
    {
        public void D(string format, params object[] args) => LogIt('D', format, args);
        public void I(string format, params object[] args) => LogIt('I', format, args);
        public void W(string format, params object[] args) => LogIt('W', format, args);
        public void E(string format, params object[] args) => LogIt('E', format, args);

        public void Suspend()
        {
            LogIt('D', "StoreFileLogger.Suspend()");
            _blnContinue = false;
            _event.Set();
        }

        public void GetBackToWork()
        {
            _queue.Clear();
            _blnContinue = true;
            Task.Run(() => FileLogTask(this));

            LogIt('D', "StoreFileLogger.GetBackToWork()");
        }

        public StoreFileLogger(string fileName)
        {
            _fileName = fileName;
            GetBackToWork();
        }

        private StoreFileLogger() { }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool isDisposed;
        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed) return;

            if (disposing)
            {
                _event?.Dispose();
            }

            isDisposed = true;
        }

        private void LogIt(char level, string format, params object[] args)
        {
            int? threadId = Environment.CurrentManagedThreadId;

            string str = Generate(DateTime.Now, threadId, level, format, args);

            lock (_queue)
            {
                _queue.Enqueue(str);
            }
            _event.Set();
        }

        private string GetNext()
        {
            lock (_queue)
            {
                return _queue.Dequeue();
            }
        }

        static private string Generate(DateTime stamp, int? threadId, char level, string format, params object[] args)
        {
            string message = string.Format(System.Globalization.CultureInfo.InvariantCulture, format, args);
            return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:HH:mm:ss.fffff}: {1}: {2,3}: {3}", stamp, level, threadId, message);
        }

        private readonly string _fileName = "logfile";
        private readonly Queue<string> _queue = new Queue<string>();
        private readonly AutoResetEvent _event = new AutoResetEvent(false);
        private bool _blnContinue;


        static private void FileLogTask(StoreFileLogger logger)
        {
            StorageFolder folder = ApplicationData.Current.LocalFolder;

            Debug.WriteLine($"FileLogTask() START - folder: '{folder.Path}', filename: '{logger._fileName}'");

            var filePath = Path.Combine(folder.Path, logger._fileName + ".txt");
            var backupPath = Path.Combine(folder.Path, logger._fileName + ".bak");

            if (File.Exists(backupPath))
            {
                File.Delete(backupPath);
            }

            if (File.Exists(filePath))
            {
                File.Move(filePath, backupPath);
            }

            while (logger._event.WaitOne())
            {
                while (logger._queue.Count > 0)
                {
                    try
                    {
                        string str = logger.GetNext();
                        if (str == null) continue;

                        Debug.WriteLine(str);

                        using var sw = File.AppendText(filePath);
                        sw.WriteLine(str);
                    }
                    catch (IOException ex)
                    {
                        Debug.WriteLine($"FileLogTask() - Exception: {ex}");
                    }
                }

                if (!logger._blnContinue)
                    break;
            }

            Debug.WriteLine("FileLogTask() END");
        }
    }
}
