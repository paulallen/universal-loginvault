﻿namespace Utils
{
    public interface ISuspendable
    {
        void Suspend();
        void GetBackToWork();
    }
}
