﻿using System;
using System.Diagnostics;

namespace Utils
{
    public class DebugLogger : ILogger
    {
        public void D(string format, params object[] args) => LogIt('D', format, args);
        public void I(string format, params object[] args) => LogIt('I', format, args);
        public void W(string format, params object[] args) => LogIt('W', format, args);
        public void E(string format, params object[] args) => LogIt('E', format, args);

        private static void LogIt(char level, string format, params object[] args)
        {
            var threadId = Environment.CurrentManagedThreadId;
            var message = Generate(DateTime.Now, threadId, level, format, args);

            Debug.WriteLine(message);
        }

        private static string Generate(DateTime stamp, Nullable<Int32> threadId, char level, string format, params object[] args)
        {
            var message = string.Format(System.Globalization.CultureInfo.InvariantCulture, format, args);
            return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:HH:mm:ss.fffff}: {1}: {2,3}: {3}", stamp, level, threadId, message);
        }
    }
}
