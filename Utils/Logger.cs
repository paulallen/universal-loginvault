﻿using System;

namespace Utils
{
    static public class Logger
    {
        static public void D(string format, params object[] args)
        {
            s_logger.D(format, args);
        }

        static public void I(string format, params object[] args)
        {
            s_logger.I(format, args);
        }

        static public void W(string format, params object[] args)
        {
            s_logger.W(format, args);
        }

        static public void E(string format, params object[] args)
        {
            s_logger.E(format, args);
        }

        static public void X(string format, Exception ex)
        {
            if (ex == null) { throw new ArgumentNullException(nameof(ex)); }

            s_logger.E($"{format} - {ex.GetType()}: '{ex.Message}'");

            // 5/2/15 Max loop
            int n = 0;
            while (((ex = ex.InnerException) != null) && (++n < 5))
            {
                s_logger.E($"{format} - InnerException: '{ex.Message}'");
            }
        }

        static public void Suspend()
        {
            if (s_logger is ISuspendable isl)
            {
                isl.Suspend();
            }
        }

        static public void Resume()
        {
            if (s_logger is ISuspendable isl)
            {
                isl.GetBackToWork();
            }
        }

        static public void SetLogger(ILogger logger)
        {
            if (logger == null) { throw new ArgumentNullException(nameof(logger)); }

            s_logger.D($"SetLogger() - '{logger.GetType()}', is ISuspendable: {logger is ISuspendable}");
            s_logger = logger;
        }

        static private ILogger s_logger = new DebugLogger();
    }


#if false
        /// <summary>
        /// Example method for reading app.config appSettings
        /// </summary>
        static public ILogger GetConfiguredLogger()
        {
            ILogger il = new DebugLogger();

            try
            {
                string logger = ConfigurationManager.AppSettings["LoggerClass"];
                if (logger != null)
                {
                    if (logger.Equals("DesktopFileLogger"))
                    {
                        string file = ConfigurationManager.AppSettings["File"];
                        il = new DesktopFileLogger(file);
                    }
                    else if (logger.Equals("WindowsEventLogger"))
                    {
                        string source = ConfigurationManager.AppSettings["Source"];
                        string name = ConfigurationManager.AppSettings["Name"];
                        il = new WindowsEventLogger(source, name);
                    }
                    else if (logger.Equals("TraceLogger"))
                    {
                        il = new TraceLogger();
                    }
                }
            }
            catch (ConfigurationErrorsException) {}

            return il;
        }
#endif
}
